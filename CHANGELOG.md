Please view this file on the [master branch](https://gitlab.com/JOGL/frontend-v0.1/-/blob/master/CHANGELOG.md), as it's out of date on other branches.

### 2020-09-25

- New: add gender and birth year on user form. They are mandatory but visible only to the user
- New: changelog link on footer
- Tweak: add skills tags on some project & group cards + on more people cards
- Tweak: force user to follow the object when creating it
- Fix: add loader on button to accept project to a challenge

### 2020-09-21

- New: create a feed page with all the post activity from all JOGL objects (/feed)
- New: if you don't follow anything, show you the feed of all JOGL activity on homepage and show an explanation message to encourage you to follow objects
- New: FAQs page accessible from the footer
- New: New design for skills/resources tags/chips
- New: add tooltip to chip component if value has been cut because it was too long
- Tweak: add JOGL user card in on-boarding page, to encourage following it
- Tweak: track deletion of user/project/groups to GA + when a user marks a need as 'completed'
- Tweak: hide follow button of peopleCard on mobile
- Fix: prevent 'enter' key press outside of the submit btn form triggering all forms

### 2020-09-15

- New: start recording events to GA on following actions: clap + follow + save + share + join + need create/update + signup and signin + user contact + user invite + project/group creation + profile completion + post/comment creation/update + click on all links with A component
- Tweak: add disabled state to the "load more" button of feeds when it's loading
- Tweak: hide load more button of home feed when it's empty
- Tweak: temp fix to hide users that didn't confirm their account from the members search page
- Tweak: show loading indicator on object feeds
- Tweak: restrict posting on program to only admins
- Tweak: add user's nickname to all peopleCard (except in clappers modal)
- Fix: allow overflow on modal to add social networks accounts

### 2020-09-08

- New: show tooltip when hovering some elements to indicate better what they are (follow/save buttons, user's name when hovering image, sdgs individual explanation..)
- Tweak: make country mandatory for user
- Tweak: show pending projects/members first on edition page of objects
- Tweak: add loading indicator while modal to submit project to challenge is loading your projects
- Tweak: most tooltips now also show on keyboard focus (accessibility)
- Tweak: minor changes in user edit form/fields (position, logic)
- Tweak: now can delete documents with keyboard navigation
- Fix: challenge form validation mandatory fields

### 2020-09-01 (accessibility !230)

- New: all elements now fully accessible and navigable/focused on tab key press
- New: all elements/buttons that triggered function on click now also triggers on 'enter' key press
- Tweak: remove outline style when focusing on all elements, except if user is using tab key to navigate (for accessibility)
- New: add focus style in some places it was missing like user form (upload input, toggle, sdg picker..)
- Fix: not being able to leave the quillJs editor when navigating with keyboard
- Fix: not being able to delete a document from a post (closes #62)
- Fix: short_bio overflowing the user card if user had a long link as short_bio
- Fix: cards not having white background on mobile

### 2020-08-28

- New: select dropdowns with better design and most now have search field!
- New: now use dropdown with search instead of input field for user's country and city
- New: adding/inviting a member to an object is now easier and more clear!
- New: when displaying an object members list, show a search bar to quickly find members, if there are more than 30 members
- Tweak: selecting skills and resources as "tags" is now easier then ever!
- Tweak: better management of members and changing their role in objects edition pages
- Tweak: better management of challenges in the program edition page
- Tweak: better management of projects in the challenge edition page
- Tweak: when completing forms, force scroll to top of success page
- Tweak: better accessibility by adding back focus outline on elements(forms etc.)
- Tweak: more fluid experience/ux when sending a contact message to another user
- Tweak: show user 'complete profile' submit button only when all inputs have been filled
- Tweak: slight refactor of card and modal components to allow overflow when you want
- Tweak: add message below user country dropdown asking them to re-select their country if it's not part of the new countries list we have
- Tweak: disable button of the modal to submit project to challenge, when it's submitting
- Tweak: make user short_bio mandatory and with max-characters + increase max characters of most object short description
- Fix: needs not showing right dates/urgency if the due date was past
- Fix: not being able to press return on user edit page textarea inputs

### 2020-08-14

- New: user can now add external/social links to their profile!
- New: can now add faq to challenges
- New: any user can now post on a need
- Tweak: group some field of user edit page on desktop
- Tweak: add gitlab icon to footer and add a text with link to our gitlab issue creation page, if a user find a bug or have an idea
- Tweak: update gitlab README & issue templates
- Fix: prevent "enter" keypress to submit user form

### 2020-08-08

- New: now display date of notification
- New: added slug(short_title) at the end of the url of projects, groups and users (when available - works with or without the /slug - ex: /project/7/theSuperProject, or/user/42/johndoe)
- New: can now view user full profile image by clicking on it
- Tweak: better banner sizes for challenge/program
- Tweak: smaller line-height for title in sticky heading of prog/chal pages (for long titles)
- Tweak: optimization on need page to display members, and now have a modal that show all of them (if more then 6)
- Tweak: clap count now correct of first clap (before you sometime had to unclap/reclap to see new count)
- Tweak: fixed unwanted scrolling bar on a post comment section
- Tweak: add margin and hover effect on image that have link, in quillJs editors
- Tweak: when you create a project/group, a slug(short_title) is now generated as you type the title
- Tweak: show different 'need to join to post on feed' message depending if the object is private or public
- Tweak: minor improvements on user page on mobile
- Tweak: now send full url to google analytics (with query params etc.)
- Tweak: add a loading indicator to the submit button of the 'complete profile' page
- Tweak: jump you at the top of the page when changing page in search page
- Tweak: better height for notification bar on really small screens
- Fix about tab not showing if program didn't have enablers/supporters
- Fix: minor fix/improvements on need page

### 2020-07-31

- New: new design for challenge page!
- New: remove infinite loader from all feeds
- New: follow the object when you join it
- New: when a user submits their project to a challenge, make them join the challenge (if they're not yet), follow it, and follow its program as well
- New: can now add external links to challenges
- New: component to quickly invite someone to an object with their email, added on program and challenge pages
- New: can now copy comment link
- Tweak: add back 'copy link' in post '...' action menu
- Tweak: 'from need' of a post now redirects you to the single need page
- Tweak: force keeping current scroll position when clicking on any 'view more' button of feeds and of the notifications page
- Tweak: each time you change tab in a program page, scroll you at the top of the tab
- Tweak: if program/challenge url specify a tab, go to this tab on page load
- Tweak: no margin for challenge banner on tablet/mobile
- Tweak: smaller margins for program/challenge edition page on mobile
- Tweak: remove unnecessary fields from challenge form, and order them better
- Tweak: use logo instead of banner in challenge mini card of edit page
- Tweak: in program about tab, move supporters next to description in main content + to the top of the right bar
- Fix: program board not working
- Fix: update list of attached documents of a post when attaching or removing them (before you had to type some text to see the changes)
- Fix: members modal of project/group not showing members if you click on it too fast

### 2020-07-25

- Tweak: in members tab of an object edition page, don't show dropdown of roles if member you want to change is owner (except if you are the owner), to prevent admins changing owner's role + prevent admin from changing their roles to 'owner'
- Tweak: add loader to the comment button, on submit + hide comment button after submit
- Tweak: make 'post' button disabled if post content is empty
- Fix: removing member from object now working again

### 2020-07-23 ([MR 210](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/210), [MR 211](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/211), [MR 212](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/212), [MR 214](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/214), [MR 215](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/215))

- New: implemented pagination with infinite scrolling on program members tab + challenge members tab
- New: can now filter challenges of search page by number of members, projects and needs
- New: Notification page with all of them
- Tweak: Notification bar: new design for the bar and the notif counter + display "99+" if more than 99 notifications + use color to indicate read/non-read instead of circle + add button to mark all your notifications as read + a see more button
- Tweak: Settings page: better design + add explanation to each notification and a general explanation
- Tweak: Better spacing for user menu links on mobile
- Tweak: improve follow bell & bookmark 'buttons' by changing icon on hover to show you what the next icon will be after action + add a loader while the request is made
- Tweak: hide post and volunteer count from a need card if they are 0
- Tweak: make title of project, challenge and groups text aligned on center (for mobile)
- Tweak: add cancel button in the form to update posts and comment, next to the update button (instead of in the '...' action menu)
- Tweak: don't show projects that have a pending submission to a challenge, in the program page
- Tweak: make french fields of program and challenges not mandatory
- Tweak: add loading indicator to program form submit button
- Tweak: better manage case where program don't have boards or challenges (about tab)
- Tweak: prevent non admin of challenge from posting in the feed
- Tweak: increase max width of some modals + better grid for objects inside some modals
- Fix: quick fix to display full date instead of relative date time on Safari and IE
- Fix: card height being weird on safari
- Fix: tabs on objects modal not showing on safari & really small screens
- Fix: some margins of program right side column

### 2020-07-10

#### Big features

- Introducing in-site notification! Access them through your menu and mange them in the settings page.
- Better management of notification (choose which you want to receive, choose between in-site and/or email notification)
- You can now save objects, and can view them via your user dropdown menu
- On the user dropdown menu, a user now have a quick access to their objects (ones he's leader of and ones he's admin of)
- New program page! Refreshed design with a logo and 3 columns on desktop. Better display of information. New component such as: carousel of objects, FAQ’s, resources, boards, external links, meeting info, supporters, contact email.
- New mobile navbar (menu)
- You can filter the program needs, participants & projects, per challenge.
- transform almost all follow buttons into bell icon next to object’s title
- you can now add external links (most common social medias or custom site..) to project & groups
- new way to display a challenge’s status advancement: on challenge mini card + on challenge header
- on a project edit page, user can now see which challenge the project is participating to, which are pending, and he can cancel participation to any of them, at any time/
- the share modal now support post
- Faster website overall

#### MAIN changes

- transform share boxes into a popup modal, and add a ‘copy link’ button (thus removing it from the ‘…’ menus button)
- show small heading with program logo and title when scrolling past program header + program nav on mobile is now sticky top when attaining it
- in the modal to submit project to challenge: add challenge program name below the name of the challenge + hide challenges with any other status then « accepting »
- in the project edit page’s modal to submit your project to a challenge, don't show challenges that are not attached to a program
- on challenge header and edit page, show submit button only if status of challenge is accepting projects and if the challenge is attached to a program
- fix modal to submit project to a challenge: show only projects you are admin of (instead of all the ones you are member of) + also show in disabled state projects you submitted but are still pending to be accepted in the challenge)
- if single post page has a #comment-id in its url, highlight the comment and scroll to it
- component with admins/leaders on faq card, where you can contact them
- new latest announcement vertical component with minimal postDisplay component
- add projects_count and mutual connections on user card and on its profile about tab

#### New design

- New cards for all objects, with better management of card format, and showing/hiding datas, and with harmonized fonts, spacing, compact style..
- change way to display members/participants: full list in program page(no more modal), full list with leader/admin badges in challenge page (no more modal), full list with leader/admin badges in projects/groups pages with leader/admin badges
- new design for posts and better display claps and comments
- new header and different style for mobile menu (right sidebar)
- new modals everywhere, with new style for modal and its close button
- better design for program page (header, cards, save button etc., new nav on desktop)
- on search page, move search bar next to the filters

#### Secondary

- continue replacing bootstrap, (ex: like button with the new Button primitive component)
- fix needCreate not refreshing list of needs
- refactor contact form modal
- change position of fields in forms (user, group, chall, prog)
- if you join a private object, text of button now show ‘pending’
- fix native share modal not sharing the correct link
- don't show comment section on post cards and redirect to post single page when clicking on comments count
- better grid for interests/sdgs on forms and to show them
- add missing default message and placeholder everywhere
- add programs and challenges filters on project search tab
- better manage cases where objects are loading or empty on program page
- can choose a featured program on homepage in production (else always show first program)
- add possibility to specify maxCol of the needs grid
- Try as much as possible to fetch data from the parent, and give it to the child component
- now can only edit a need from its single page, not the card.
- Better pages for single post and need
- new way to manage and call chips + better chip style + same title style for all cards
- smaller contact button for people card
- better manage case when modal has no title
- better img height for cards
- improve all padding and margin of all tabs (desktop+mobile)
- message modal title's now includes full name of user
- sticky nav and some elements of right col (desktop)
- add due and end_date on single need as well as its card
- transform need content into quill
- search page filter now accepts 2 types of status (from need and from challenge)
- now redirects you to complete-profile if you have not filled everything (at each sign in)
- if a challenge status is draft & we are not admin, redirect user to « all challenges » page
- show published and due date in need card and need single page (if we have them) + show due date conditionally depending on days left
- hide need due date if need status is 'completed’

#### New dev components

- Create a Box component that is universal to manage spacing, and many more
- Create primitive A, H2, continue Program Home tab, add fonts to the theme
- New carousel component
- Create Accordion component (for FAQs…)
- New grid component to display list of objects in a grid style
- New Filters component that let you filter objects from a grid (for ex)

#### DEV/Fixes

- migrating to styled component in many pages/components
- Now have a theme file that manages all the theme style (colors, shadows, breakpoints, fontSizes..)
- faster api calls, less fetches, and more caching..
- moved to Typescript and type most of the components/pages
- Massive refactoring of many components
- Start deleting jQuery where we could
- improve management of plural strings, with a dedicated file with all of them
- code cleaning, and reduced calls
- fix delete button of some forms triggering update
- fixed infinite scroller on feeds
- don't display draft challenges in program challenges + don't display status circle component in draft challenge's page and card

### 2020-05-10

- New: add needs and programs tabs in user following modal + fixes for this modal
- New: users can now delete posts and comments of objects which they are admin of (their user feed, or project/group feed)
- New: make join button display 'pending' if you joined a private object
- Tweak: harmonize quill editor with html display
- Tweak: can now better delete need workers
- Tweak: improve performance of getting featured program on connected homepage
- Fix: modal not closing when opening a project in the challenge projects modal
- Fix: 404 page
- Fix copy need link getting wrong url
- Fix error when accessing a user page when not connected

### 2020-05-08

- New: add needs and programs tabs in user following modal + fixes for this modal
- New: users can now delete posts and comments of objects which they are admin of (their user feed, or project/group feed)
- New: make join button display 'pending' if you joined a private object
- Tweak: harmonize quill editor with html display
- Tweak: can now better delete need workers
- Tweak: improve performance of getting featured program on connected homepage
- Fix: modal not closing when opening a project in the challenge projects modal
- Fix: 404 page
- Fix copy need link getting wrong url
- Fix error when accessing a user page when not connected

### 2020-04-27

- New: Added spanish - Bienvenido a los españoles!
- Tweak: Synchronize login: If you login when having multiple jogl tabs open, it logs you in all of them
- Tweak: remove "?" at the end of the url on search pages
- Tweak: Default 'filter by' of membebrs is back to 'date added(recent)'
- Fix: french translation of sdgs not showing

### 2020-04-23

- New: Migrated to Next.js!
- New: Now have dynamic title, description and image when sharing a link on social medias (a user, project, program..)
- New: Post restriction rules: can't post on project/group if you're not a member + can't post on challenge/programs if you're not admin
- Tweak: Faster loading time
- Tweak: back to showing all interests/sdgs on object edition pages
- Tweak: add message saying you need to join a need to post in it
- Tweak: better date display in posts/comments (+ auto-refresh)
- Tweak: if you have multiple tabs open, on logout, logout from all tabs
- Tweak: add skills/sdgs/resource title to better understand what the tags are (on objects headers)
- Tweak: add loading spinner to all "submit" buttons

### 2020-03-30

- New: New search page, that will replace projects/challenges/users/groups pages
- New: You can now decide to receive mail notification or not
- Tweak: filter now show sdgs cards instead of sdg name
- Tweak: Search page improvements (horizontal scrollable nav bar)
- Tweak: Search page style improvements for mobile
- Tweak: on search page, added "no result" in case of no result
- Tweak: now get need members from /members api, instead of from the single need api
- Tweak: new placeholder for search page search bar
- Tweak: add translations for show more filters button
- Fix: members add modal not searching users
- Fix: mentions not working on post and quill editor
- Fix: not able to add skills to objects anymore
- Fix: skills tag spacing issue

### 2020-03-26

- New: Now can add hooks on projects that trigger on certain events
- Tweak: Added loading indicator for members/participants modal
- Tweak: Add loading indicator to single post and need
- Tweak: always use/redirect program and challenge links to human readable url instead of id
- Tweak: use small banner url when calling for small banner image
- Tweak: Bigger logo url for post/need/comment creator
- Fix: now have right object name when sharing an object through mail

### 2020-03-23

- New: added german language!
- New: added participants modal in program header
- Tweak: optimize loading time, by getting objects members/participants only once the page has fully loaded
- Tweak: improve user header and needs card
- Tweak: harmonize challenge & program headers
- Tweak: allow admins to delete posts of their objects (posts/needs..)
- Tweak: improve design for needs cards and their single page
- Tweak: remove rules read confirmation when submitting project to a challenge
- Tweak: more space for needs on mobile
- Tweak: add link to project on single need
- Fix: some links overflowing on needs description and post/comment input field

### 2020-03-18

- New: page to see a single need
- New: added share buttons to need object
- New: add featured program card on homepage
- Tweak: add needs & projects tab on challenge and program pages
- Tweak: added share button on single project, group, challenge, program & need card
- Tweak: different design depending on where the need comes from
- Tweak: new dropdown menu for need: to edit, delete & copy its link
- Tweak: Better share buttons modal position
- Tweak: use small profile logo url when we don't need good quality
- Fix: get project clap state in projects page
- Fix: hide load button if feed has less than 5 posts

### 2020-03-16

- New: projects page with ability to search and filter easily
- Tweak: improve image size management for projects/communities banner images
- Tweak: Code improvements/harmonization
- Fix: bug when contacting someone from a popup (desktop only)
- Fix: inviting members to object not working

### 2020-03-06

- New: Show meta data of first link of a post
- New: Ability to copy any post link
- New: New page to see a single post
- New: Ability to report posts & comments
- New: Now can remove user avatar or banner to reset it to default
- Tweak: Hide nb of users on people page + bigger font size for filters
- Tweak: Force to show all sdgs on complete profile page
- Tweak: Add icons to user menu dropdown
- Tweak: Added confirmation message on banner/avatar removal
- Tweak: in members modal from projects/group/challenges pages, separate members into tabs depending on their type
- Tweak: default hits for search page is now 15
- Tweak: Rename all search index urls to plural (ex: ?i=Project -> ?i=Projects)
- Fix: alignments & translation fixes
- Fix: Clear filters not working on mobile

### 2020-02-17

- New: Mentioning now display name/title instead of short name/title
- Tweak: Different design for mention when displaying or editing
- Tweak: Added a confirmation message box on the sign in page, when validating your account via mail
- Tweak: Code structure: reorder and remove some files/folders
- Tweak: Apply new mention system to quill js editor
- Tweak: Update changelog
- Fix: Remove margin bottom on all authentification pages (to make it full height)

### 2020-01-24

- New: added ability to mention users and projects within QuillJS editor
- Tweak: improve mentioning on posts and comments with a list displaying suggested user/project image, name and nickname/shortname
- Tweak: new style for header search bar results, now display user/project image
- Fix: image size now saving correctly on quill js editor

### 2020-01-14

- New: Users page, blazing fast, with search bar, filters and pagination
- New: Infinite loading in projects & groups page + in all feeds
- New: Modal of people who clapped a post
- Tweak: Improved design for list of users in modals (followers, following, members..)
- Tweak: Now load 12 items at a time (page projects & groups)
- Tweak: Separate objects into tabs, inside user following modal (https://gitlab.com/JOGL/JOGL/issues/247)
- Tweak: Improved profile page header on mobiles
- Fix: Post content inside () being deleted on submit
- Tweak: Sticky nav bar to the top, on user profile page
- Tweak: Show only 8 sdgs by default on an object edition (and can toggle to view more/mess)

### 2019-12-07

- New: Refreshed design for the "need" object
- New: added 12 new default profile images
- New: on all feeds : get only a few elements from the api & add a load more button to view more
- Tweak: change emails "from name" to "JOGL - Just One Giant Lab" (instead of JOGL) + "from email" to "noreply@" (instead of hello@)
- Tweak: changed max size for banners and profile image from 10Mo to 4Mo
- Tweak: allow only png/jpg as banner or profile images
- Tweak: remove unnecessary objects from most api calls (speed improvement)
- Tweak: now order projects/groups/needs in the backend (so no need to filter them on front anymore)
- Tweak: added pagination in consequence (to get 5 most recent projects/groups.. for ex)
- Fix: sort comments by id, so have most recent on top
- Fix: fixed list component (ul/li) not working in quill js editor

### 2019-12-02

- New: Add notification when someone posts on your challenge or program
- New: Added more field on the project object (to guide you detailing it even more if you want)
- Tweak: All quill js links will now open on new window (https://gitlab.com/JOGL/JOGL/issues/158)
- Tweak: on all about pages, detect links outside of <a> tag and linkify them
- Tweak: improve user profile header’s style on really small devices
- Tweak: don't trigger email if you commented on your profile
- Tweak: don't trigger email if you comment on a post in which you are mentioned
- Tweak: now redirect to correct tabs depending on hash of a profile page
- Tweak: in emails, change wording community to group
- Tweak: replace due date by 'urgent' in the need
- Tweak: in the confirmation email, add full confirmation link , if the button doesn’t work
- Tweak: change email content if user mentioned someone in a post or pot’s comment on his profile
- Tweak: don't trigger email if you posted or commented in an object you are admin/owner
- Tweak: change email content when someone you follow post in their own profile (else they are posting in other object you follow)
- Fix: Home feed not working if you followed deleted users/projects..
- Fix: bug when switching to another tab in an object edition page

### 2019-11-21

- New: Added needs in menu and display latest needs on homepage
- New: tabs on mobile homepage, to switch between the "feed" and "what's new" sections
- Tweak: specify on a project header if it's participating to a challenge
- Tweak: open upload to all file types!
- Tweak: Make user bio go full width if user is not connected (on desktop)
- Tweak: add '...' if text is longer than 2 lines (on members cards, and on projects/groups small cards on homepage)
- Tweak: Style improvements for really small devices (header menu + homepage)
- Fix: On the 'someone followed you' mail, redirect the button to the person who followed you, not you.

### 2019-11-19

- New: New notification emails: when someone follows you, when someone post in an object you follow (user, project, challenge..).
- New: homepage (when connected) loads only 10 posts and you can load more with button (improve performance)
- Tweak: Speed improvements through many pages
- Tweak: now specify which user mentioned or posted about you in notification emails.
- Tweak: Design enhancements on needs page
- Tweak: view all user's skills when clicking on the "+.." on the user's profile header
- Tweak: better display documents on feed posts
- Fix: sign in modal not closing when clicking the button
- Fix: language switcher not displaying selected language

### 2019-11-18

- New: You can now contact users on the platform! (if they chose to be publicly contacted)
- New: Brand new notification emails design!
- New: New design when listing members on the platform (people page, member modal popups..) + can contact users from modals as well.
- New: added 8 default profile images that are chosen randomly on sign up (if you don't upload a profile picture)
- New: Now trigger a "please sign in" modal on any 401 error (except on sign in page)
- New: Add a page with the needs from all the projects (/needs)
- New: Now can edit and add documents to an old post
- New: Now can delete documents on your project/challenge
- New: New people page that shows only 100 users at a time and have a "load more button"
- Tweak: Those pages now load much faster: projects, challenges, communities and homepage.
- Tweak: Revised wording, subject, and content for all email notifications
- Tweak: Dynamic hash in url that goes to matching tab, and update when you select another tab
- Tweak: Added smooth scrolling when jumping to other tab, sections..
- Tweak: More precise error messages on sign in + display button to resend confirmation link if you didn’t validate your account
- Tweak: Improve the modal "Add Project"
- Tweak: Automatically follow a need when creating it
- Fix: On the homepage feed, posts from « needs » object now redirect to the need’s tab section of its project page
- Fix: various fixes on the feed
- Fix: various minor fixes

### 2019-10-18

- New: Add changelog
- New: User can now download their data on the settings page
- New: new design with fewer and sticky tabs for normal and edition pages of the following object: project, user, groups, programs & challenges
- New: Dynamic no result message
- New: Remove followers and members tab (project, groups, users), now accessible by clicking the numbers in header
- Tweak: only display city and country for address
- Tweak: revised settings page design
- Tweak: code optimization
- Tweak: better design for document cards
- Tweak: now anyone can post or comment on a need
- Tweak: hide documents tab is user is not admin and they are no docs
- Tweak: added needs in the project edition page
- Tweak: anyone can comment a need (no need to click I'll help)
- Tweak: Better wording for need object and other places
- Tweak: Display number of posts and comments on the need card
- Tweak: Display confirmation message on member role change

### 2019-10-11 ([MR 167](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/167))

- New: some fields for challenges and program now have french translation
- New: you can now archive or delete a project (https://gitlab.com/JOGL/JOGL/issues/195))
- New: insert emoji directly within our text editor (https://gitlab.com/JOGL/JOGL/issues/200)
- Tweak: show need count on projects cards
- Tweak: added a button to show/hide some fields in the edition pages
- Tweak: don’t show draft challenges anywhere, show them only when published (https://gitlab.com/JOGL/JOGL/issues/226)
- Tweak: code optimisation and cleaning
- Tweak: show more results in search bar and better readability
- Tweak: add gitlab link to contribute to the code
- Tweak: add MIT license to the code (https://gitlab.com/JOGL/JOGL/issues/241)
- Fix: problem that prevent changing role to owner in french langage
- Fix: pending users not working + unability to join projects (err 403)
- Fix: minor problems

### 2019-09-12 ([MR 156](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/156))

- New: introducing the search bar on JOGL (searching into skills, names, descriptions... of projects and people) 🔎 (https://gitlab.com/JOGL/JOGL/issues/39)
- New: you can now filter people, projects and groups by alphabetical order, by dates or by popularity
- New: Need object (https://gitlab.com/JOGL/JOGL/issues/64) / (https://gitlab.com/JOGL/JOGL/issues/65) / (https://gitlab.com/JOGL/JOGL/issues/67)
- Tweak: add short bio to user profile
- Tweak: new header for challenges
- Tweak: participate dropdown on challenge header
- Tweak: better navigation on mobile devices
- Tweak: new design for search bar and mobile dropdown menu
- tweak: add link to create project & link to rules on add project modal
- Fix: some security issues
- Fix: minor problems

### 2019-08-19

- New: Add a chatbot!
- New: ability to drag and drop + copy/paste + resize images in the quill text editor
- Tweak: Add translations + revised the pages: data, terms/conditions, ethic/pledge.
- Tweak: save the page you were before signing in
- Fix security issues
- Fix: minor improvements

### 2019-07-31

- New: add language dropdown on header + help link on user menu
- Tweak: show comments by default and add button to show more than 4
- Tweak: translation ethic pledge page
- Tweak: little design enhancement of profile page
- translate home intro + enhance style for mobile
- Fix: mentions that made the whole text a link

### 2019-07-24

- Tweak: improve design of creation and edition page (responsive)
- Tweak: some pages have now dynamic tab titles
- Tweak: all website now translated
- Tweak: at some places on website, show only few skills and interests, and add a button to view more
