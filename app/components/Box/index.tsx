import {
  flexbox,
  system,
  space,
  layout,
  border,
  color,
  shadow,
  typography,
  position,
  SpaceProps,
  FlexboxProps,
  LayoutProps,
  BorderProps,
  ColorProps,
  ShadowProps,
  TypographyProps,
  PositionProps,
} from 'styled-system';
import styled from '~/utils/styled';

export interface BoxProps
  extends SpaceProps,
    PositionProps,
    LayoutProps,
    FlexboxProps,
    BorderProps,
    ShadowProps,
    TypographyProps,
    ColorProps {
  // This allows us to pass a different type of component than the default div
  // Solved with https://dev.to/jdcas89/start-your-app-the-right-way-featuring-react-styled-system-styled-components-and-typescript-7a4
  as?: keyof JSX.IntrinsicElements | React.ComponentType<any>;
  // Must add color or it will throw an error...
  color?: string;
  shadow?: boolean;
  row?: boolean;
  spaceX?: SpaceProps['marginLeft'];
  spaceY?: SpaceProps['marginTop'];
}
const Box = styled.div<BoxProps>`
  display: flex;
  flex-direction: ${(p) => (p.row ? 'row' : 'column')};
  ${(p) => p.shadow && `box-shadow: ${p.theme.shadows.default}`};
  /* this is called the owl selector. It selects all elements expect the first one */
  > * + * {
    ${system({
      spaceX: {
        property: 'marginLeft',
        scale: 'space',
      },
    })}
    ${system({
      spaceY: {
        property: 'marginTop',
        scale: 'space',
      },
    })}
  }
  ${[flexbox, space, layout, border, color, shadow, typography, position]};
`;
export default Box;
