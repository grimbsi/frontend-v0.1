import { Component } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';
import Link from 'next/link';
/** * Form objects ** */
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import FormImgComponent from '~/components/Tools/Forms/FormImgComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormTextAreaComponent from '~/components/Tools/Forms/FormTextAreaComponent';
import FormWysiwygComponent from '~/components/Tools/Forms/FormWysiwygComponent';
/** * Validators ** */
import FormValidator from '~/components/Tools/Forms/FormValidator';
import challengeFormRules from './challengeFormRules.json';
/** * Images/Style ** */
interface Props {
  mode: 'edit' | 'create';
  challenge: any;
  sending: boolean;
  handleChange: (key, content) => void;
  handleSubmit: () => void;
}
class ChallengeForm extends Component<Props> {
  validator = new FormValidator(challengeFormRules);

  static get defaultProps() {
    return {
      mode: 'create',
      challenge: {
        banner_url: '',
        description: '',
        faq: '',
        followers: 0,
        interests: [],
        public: true,
        rules: '',
        short_title: '',
        short_description: '',
        short_description_fr: '',
        skills: [],
        status: 'draft',
        short_name: '',
        title: '',
        title_fr: '',
      },
      sending: false,
    };
  }

  handleChange(key, content) {
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation[`valid_${key}`] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit() {
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.challenge);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      let firstError = true;
      Object.keys(validation).forEach((key) => {
        if (key !== 'isValid') {
          if (validation[key].isInvalid && firstError) {
            // if field is invalid and it's the first field that has error
            const element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 130; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to element to show error
            firstError = false; // set to false so that it won't scroll to second invalid field and further
          }
          stateValidation[`valid_${key}`] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderBtnsForm() {
    const { challenge, mode, sending } = this.props;
    let urlBack = '/search/challenges';
    let urlBackAs = '/search/challenges';
    let textAction = 'Create';
    if (mode === 'edit') {
      urlBack = '/challenge/[short_title]';
      urlBackAs = `/challenge/${challenge.short_title}`;
      textAction = 'Update';
    }

    return (
      <div className="row challengeFormBtns">
        <Link href={urlBack} as={urlBackAs}>
          <a>
            <button type="button" className="btn btn-outline-primary">
              <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
            </button>
          </a>
        </Link>
        <button
          type="button"
          onClick={this.handleSubmit.bind(this)}
          className="btn btn-primary"
          disabled={sending}
          style={{ marginRight: '10px' }}
        >
          {sending && (
            <>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
              &nbsp;
            </>
          )}
          <FormattedMessage id={`entity.form.btn${textAction}`} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } = this.state
      ? this.state
      : '';
    const { challenge, mode, intl } = this.props;

    return (
      <form className="challengeForm">
        <FormDefaultComponent
          content={challenge.title}
          errorCodeMessage={valid_title ? valid_title.message : ''}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.title', defaultMessage: 'Title' })}
          placeholder={intl.formatMessage({
            id: 'challenge.form.title.placeholder',
            defaultMessage: 'A great challenge',
          })}
        />
        <FormDefaultComponent
          content={challenge.title_fr}
          id="title_fr"
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.title_fr', defaultMessage: 'Title fr' })}
          placeholder={intl.formatMessage({
            id: 'challenge.form.title.placeholder',
            defaultMessage: 'A great challenge',
          })}
        />
        <FormDefaultComponent
          content={challenge.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ''}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory
          pattern={/[A-Za-z0-9]/g}
          title={intl.formatMessage({ id: 'entity.info.short_name', defaultMessage: 'Short Name' })}
          placeholder={intl.formatMessage({
            id: 'challenge.form.short_title.placeholder',
            defaultMessage: 'agreatchallenge',
          })}
          prepend="#"
        />
        <FormTextAreaComponent
          content={challenge.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ''}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory
          maxChar={340}
          onChange={this.handleChange.bind(this)}
          rows={5}
          title={intl.formatMessage({ id: 'entity.info.short_description', defaultMessage: 'Short description' })}
          placeholder={intl.formatMessage({
            id: 'challenge.form.short_description.placeholder',
            defaultMessage: 'The challenge briefly explained',
          })}
        />
        <FormTextAreaComponent
          content={challenge.short_description_fr}
          id="short_description_fr"
          maxChar={340}
          onChange={this.handleChange.bind(this)}
          rows={3}
          title={intl.formatMessage({ id: 'entity.info.short_description_fr', defaultMessage: 'Short description fr' })}
          placeholder={intl.formatMessage({
            id: 'challenge.form.short_description.placeholder',
            defaultMessage: 'The challenge briefly explained',
          })}
        />
        {mode === 'edit' && (
          <>
            <FormWysiwygComponent
              id="description"
              title={intl.formatMessage({ id: 'entity.info.description', defaultMessage: 'Description' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.description.placeholder',
                defaultMessage: 'Describe the challenge in detail, with formatted text, images...',
              })}
              content={challenge.description}
              onChange={this.handleChange.bind(this)}
              show
            />
            <FormWysiwygComponent
              id="description_fr"
              title={intl.formatMessage({ id: 'entity.info.description_fr', defaultMessage: 'Description fr' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.description.placeholder',
                defaultMessage: 'Describe the challenge in detail, with formatted text, images...',
              })}
              content={challenge.description_fr}
              onChange={this.handleChange.bind(this)}
            />
          </>
        )}

        <FormInterestsComponent
          content={challenge.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ''}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: 'entity.info.interests', defaultMessage: 'Interests' })}
        />
        <FormSkillsComponent
          content={challenge.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ''}
          id="skills"
          type="challenge"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({
            id: 'general.skills.placeholder',
            defaultMessage: 'Big data, Web Development, Open Science...',
          })}
          title={intl.formatMessage({ id: 'entity.info.skills', defaultMessage: 'Expected skills' })}
        />
        {mode === 'edit' && (
          <>
            {/* <FormWysiwygComponent
              id="rules"
              title={intl.formatMessage({ id: 'entity.info.rules', defaultMessage: 'Rules' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.rules.placeholder',
                defaultMessage: 'Describe the rules to be able to participate to the challenge, to submit projects...',
              })}
              content={challenge.rules}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="rules_fr"
              title={intl.formatMessage({ id: 'entity.info.rules_fr', defaultMessage: 'Rules fr' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.rules.placeholder',
                defaultMessage: 'Describe the rules to be able to participate to the challenge, to submit projects...',
              })}
              content={challenge.rules_fr}
              onChange={this.handleChange.bind(this)}
            /> */}
            {/* <FormWysiwygComponent
              id="faq"
              title={intl.formatMessage({ id: 'entity.info.faq', defaultMessage: 'Faq' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.faq.placeholder',
                defaultMessage: 'Write the FAQ...',
              })}
              content={challenge.faq}
              onChange={this.handleChange.bind(this)}
            />
            <FormWysiwygComponent
              id="faq_fr"
              title={intl.formatMessage({ id: 'entity.info.faq_fr', defaultMessage: 'Faq fr' })}
              placeholder={intl.formatMessage({
                id: 'challenge.form.faq.placeholder',
                defaultMessage: 'Write the FAQ...',
              })}
              content={challenge.faq_fr}
              onChange={this.handleChange.bind(this)}
            /> */}
            <FormDropdownComponent
              id="status"
              type="challenge"
              warningMsgIntl={{
                id: 'challenge.info.dropDownMsg',
                defaultMessage:
                  "As long as the status of this challenge is on 'Draft', it won't be visible on the platform.",
              }}
              title={intl.formatMessage({ id: 'entity.info.status', defaultMessage: 'Status' })}
              content={challenge.status}
              options={['draft', 'soon', 'accepting', 'evaluating', 'active', 'completed']}
              onChange={this.handleChange.bind(this)}
            />
            <FormImgComponent
              type="banner"
              id="banner_url"
              imageUrl={challenge.banner_url}
              itemId={challenge.id}
              itemType="challenges"
              title={intl.formatMessage({ id: 'challenge.info.banner_url', defaultMessage: 'Challenge banner' })}
              content={challenge.banner_url}
              defaultImg="/images/default/default-challenge.jpg"
              onChange={this.handleChange.bind(this)}
              tooltipMessage={intl.formatMessage({
                id: 'challenge.info.banner_url.tooltip',
                defaultMessage:
                  'For an optimal display, choose a visual in the format 2000 x 350 pixels (accepted formats: .png, .jpeg, .jpg; maximum weight: 2Mo).',
              })}
            />
            <FormImgComponent
              id="logo_url"
              content={challenge.logo_url}
              title={intl.formatMessage({ id: 'challenge.info.logo_url', defaultMessage: 'Challenge logo' })}
              imageUrl={challenge.logo_url}
              itemId={challenge.id}
              type="avatar"
              itemType="challenges"
              defaultImg="/images/default/default-program.jpg"
              onChange={this.handleChange.bind(this)}
            />
            <FormDefaultComponent
              id="launch_date"
              content={challenge.launch_date && challenge.launch_date.split('T')[0]}
              onChange={this.handleChange.bind(this)}
              type="date"
              title={intl.formatMessage({ id: 'entity.info.launch_date', defaultMessage: 'Launch date' })}
            />
            <FormDefaultComponent
              id="final_date"
              content={challenge.final_date && challenge.final_date.split('T')[0]}
              onChange={this.handleChange.bind(this)}
              type="date"
              title={intl.formatMessage({ id: 'entity.info.final_date', defaultMessage: 'Final date' })}
            />
            <FormDefaultComponent
              id="end_date"
              content={challenge.end_date && challenge.end_date.split('T')[0]}
              onChange={this.handleChange.bind(this)}
              type="date"
              title={intl.formatMessage({ id: 'entity.info.end_date', defaultMessage: 'End date' })}
            />
            {/* <FormDefaultComponent
              id="address"
              title={intl.formatMessage({ id: 'general.address', defaultMessage: 'Address' })}
              placeholder={intl.formatMessage({
                id: 'general.address.placeholder',
                defaultMessage: '5620 Impact Street',
              })}
              content={challenge.address}
              onChange={this.handleChange.bind(this)}
            /> */}
            {/* <FormDefaultComponent
              id="city"
              title={intl.formatMessage({ id: 'general.city', defaultMessage: 'City' })}
              placeholder={intl.formatMessage({
                id: 'general.city.placeholder',
                defaultMessage: 'Bangkok, Paris, Medellin...',
              })}
              content={challenge.city}
              onChange={this.handleChange.bind(this)}
            />
            <FormDefaultComponent
              id="country"
              title={intl.formatMessage({ id: 'general.country', defaultMessage: 'Country' })}
              placeholder={intl.formatMessage({
                id: 'general.country.placeholder',
                defaultMessage: 'India, Ouganda, Brazil, France..',
              })}
              content={challenge.country}
              onChange={this.handleChange.bind(this)}
            /> */}
          </>
        )}
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(ChallengeForm);
