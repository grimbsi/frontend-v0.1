import React, { useState, useRef, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import Box from '../Box';
import Grid from '../Grid';
import PeopleCard from '../People/PeopleCard';
import A from '../primitives/A';
import { useScrollHandler } from '~/utils/utils';
import { useApi } from '~/contexts/apiContext';
import QuickSearchBar from '../Tools/QuickSearchBar';
import Loading from '../Tools/Loading';

const ChallengeMembers = ({ challengeId }) => {
  const membersPerQuery = 24; // number of members we get per query calls (make it 3 to test locally)
  const { data: dataMembers } = useGet(`/api/challenges/${challengeId}/members?items=${membersPerQuery}`);
  const [members, setMembers] = useState([]);
  const { userData } = useUserData();
  const [currentPage, setCurrentPage] = useState(2);
  const [totalPages, setTotalPages] = useState(2);
  const [hasLoadOnce, setHasLoadOnce] = useState(false);
  const [offSetTop, setOffSetTop] = useState();
  const loadButtonRef = useRef();
  const [shouldResetLoadMore, setShouldResetLoadMore] = useState(true);
  const shouldLoadMore = useScrollHandler(offSetTop); // set to true if we scroll beyond the invisible "load more" button
  const api = useApi();
  useEffect(() => {
    // set offSetTop as the one from invisible loading button
    setOffSetTop(loadButtonRef?.current?.offsetTop - 200); // (remove 200px to trigger api call a bit before attaining button)
  }, [loadButtonRef?.current?.offsetTop]); // reset each time button offsetTop changes
  useEffect(() => {
    // set dataMembers from first api call, only once, or when filtering members by challenge
    if (!hasLoadOnce && dataMembers?.members) setMembers(dataMembers.members);
  }, [dataMembers]);

  useEffect(() => {
    // launch this function each time values of shouldLoadMore or shouldResetLoadMore change
    setShouldResetLoadMore(true); // reset to true each time shouldLoadMore value changes
    // had to use 'shouldResetLoadMore', else, 'shouldLoadMore' was always true on when first entering this function, making all the next api calls, more details down
    if (shouldLoadMore && shouldResetLoadMore && currentPage <= totalPages) {
      api.get(`/api/challenges/${challengeId}/members?items=${membersPerQuery}&page=${currentPage}`).then((res) => {
        setTotalPages(parseInt(res.headers['total-pages'])); // get total pages from response headers
        const nextMembers = res.data.members;
        if (members) setMembers([...members, ...nextMembers]);
        setHasLoadOnce(true); // set to true so it don't setMembers from first call
        setCurrentPage(currentPage + 1); // increment current page count
        setShouldResetLoadMore(false); // make it false so it don't enter in the if, unless user scroll (thus changing shouldLoadMore value, and entering again in this useEffect, and if)
      });
    }
    // }, [shouldLoadMore]);
  }, [shouldLoadMore, shouldResetLoadMore]);

  return (
    <>
      {!userData && ( // if user is not connected
        <Box spaceX={2} pb={4}>
          <A href="/signin" as="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toContactMembers}'}
              values={{
                toContactMembers: (
                  <FormattedMessage id="challenge.signinCta.members" defaultMessage="to contact members" />
                ),
              }}
            />
          </A>
        </Box>
      )}
      <Box position="relative">
        {/* Members grid/list */}
        {members && (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid gridGap={[4, undefined, 5]} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pb={4}>
              {members.length === 0 ? (
                <Loading />
              ) : (
                members?.map((member, i) => (
                  <PeopleCard
                    key={i}
                    id={member.id}
                    firstName={member.first_name}
                    lastName={member.last_name}
                    nickName={member.nickname}
                    shortBio={member.short_bio}
                    skills={member.skills}
                    resources={member.ressources}
                    status={member.status}
                    lastActive={member.current_sign_in_at}
                    logoUrl={member.logo_url}
                    canContact={member.can_contact}
                    mutualCount={member.mutual_count}
                    projectsCount={member.projects_count}
                    noMobileBorder={false}
                  />
                ))
              )}
            </Grid>
            {dataMembers?.members.length >= membersPerQuery && (
              // set an invisible button used as a ref to get more members when attaining it
              <Box alignSelf="center" pt={4} ref={loadButtonRef} opacity="0">
                {/* <Button>
                  <FormattedMessage id="general.load" defaultMessage="Load more" />
                </Button> */}
              </Box>
            )}
          </>
        )}
      </Box>
    </>
  );
};

export default ChallengeMembers;
