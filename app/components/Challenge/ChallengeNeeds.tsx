import React from 'react';
import { FormattedMessage } from 'react-intl';
import useUserData from '~/hooks/useUserData';
import Box from '../Box';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import P from '../primitives/P';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const ChallengeNeeds = ({ dataNeeds }) => {
  const { userData } = useUserData();
  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.needs.text"
            defaultMessage="To participate to this challenge, participate to as many of its needs. Check out the needs already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/header.signIn" as="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toFillNeed}'}
              values={{
                toFillNeed: <FormattedMessage id="program.signinCta.need" defaultMessage="to fill a need" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {!dataNeeds ? (
          <Loading />
        ) : dataNeeds?.needs.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pb={4}>
            {dataNeeds.needs.map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
              />
            ))}
          </Grid>
        )}
      </Box>
    </div>
  );
};

export default ChallengeNeeds;
