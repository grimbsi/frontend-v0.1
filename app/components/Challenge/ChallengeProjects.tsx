import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import useChallenges from '~/hooks/useChallenges';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const ChallengeProjects = ({ challengeId }) => {
  const { data: dataProjects, error: projectsError } = useGet<{ projects: Project[] }>(
    `/api/challenges/${challengeId}/projects`
  );
  const { userData, userDataError } = useUserData();
  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.projects.text"
            defaultMessage="To participate to this challenge, participate to as many of its projects. Check out the projects already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/signin" as="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toJoinProject}'}
              values={{
                toJoinProject: <FormattedMessage id="program.signinCta.project" defaultMessage="to join a project" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {!dataProjects ? (
          <Loading />
        ) : dataProjects?.projects.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} pb={4}>
            {dataProjects.projects
              // don't display pending projects
              .filter(({ challenge_status }) => challenge_status !== 'pending')
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  clapsCount={project.claps_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                />
              ))}
          </Grid>
        )}
      </Box>
    </div>
  );
};

export default ChallengeProjects;
