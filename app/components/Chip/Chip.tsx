import React, { FC, forwardRef } from 'react';
import ReactTooltip from 'react-tooltip';
import styled from '~/utils/styled';
import Box from '../Box';

interface Props {
  color?: string;
  maxLength?: number;
  children: string | string[];
  smallChips?: boolean;
  hasOpacity?: boolean;
}
// TODO: Explain why we use forwardRef here.
const Chip: FC<Props> = forwardRef(
  ({ color = 'black', children, maxLength = 1000, smallChips, hasOpacity = false }, ref) => {
    // add "..." to value if it's length is > than maxLength
    const value = children.length > maxLength ? `${children.slice(0, maxLength)}...` : children;
    return (
      <>
        <Container
          ref={ref}
          border={`1px solid #CED4DB`}
          backgroundColor={color}
          // color="#5C5D5D"
          color="#494a4a"
          borderRadius={smallChips ? '1rem' : '1rem'}
          height={smallChips ? '1.75rem' : '2.2rem'}
          fontWeight={smallChips ? '400' : '500'}
          justifyContent="center"
          alignItems="center"
          px={2}
          fontSize={smallChips ? 1 : '.9rem'}
          mr={smallChips ? 2 : '.65rem'}
          mb={smallChips ? 1 : '.6rem'}
          opacity={hasOpacity ? 0.65 : 1}
          hasOpacity={hasOpacity}
          // add tooltip prop to show chip tooltip, it its length is more than maxLength
          {...(children.length > maxLength && { 'data-tip': children, 'data-for': `skillTooltip${children}` })}
        >
          {value}
        </Container>
        <ReactTooltip
          id={`skillTooltip${children}`}
          effect="solid"
          role="tooltip"
          type="dark"
          className="forceTooltipBg"
        />
      </>
    );
  }
);

const Container = styled(Box)`
  white-space: nowrap;
  &:hover {
    opacity: ${(p) => (p.hasOpacity ? 1 : 0.8)};
  }
`;

export default Chip;
