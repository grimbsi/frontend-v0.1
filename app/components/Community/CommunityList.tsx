import Grid from '../Grid';
import CommunityCard from './CommunityCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

export default function CommunityList({ listCommunities, cardFormat = undefined, gridCols = [1, 2, undefined, 3] }) {
  return (
    <Grid gridGap={4} gridCols={gridCols} display={['grid', 'inline-grid']} pt={3}>
      {!listCommunities ? (
        <Loading />
      ) : listCommunities.length === 0 ? (
        <NoResults type="group" />
      ) : (
        listCommunities.map((community, i) => (
          <CommunityCard
            key={i}
            id={community.id}
            title={community.title}
            shortTitle={community.short_title}
            short_description={community.short_description}
            members_count={community.members_count}
            clapsCount={community.claps_count}
            has_saved={community.has_saved}
            // show skills only if cardFormat is not compact
            {...(cardFormat !== 'compact' && { skills: community.skills })}
            banner_url={community.banner_url || '/images/default/default-group.jpg'}
            cardFormat={cardFormat}
          />
        ))
      )}
    </Grid>
  );
}
