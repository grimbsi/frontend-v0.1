import { useState, useEffect, useCallback } from 'react';
import { FormattedMessage } from 'react-intl';
import PostDisplay from '~/components/Feed/Posts/PostDisplay';
import PostCreate from './Posts/PostCreate';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import useUserData from '~/hooks/useUserData';
import Button from '../primitives/Button';
import Alert from '../Tools/Alert';

export default function MyFeed({ displayCreate = true, feedId }) {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [isFeedEmpty, setIsFeedEmpty] = useState(false);
  const [hideLoadBtn, setHideLoadBtn] = useState(true);
  const [loadBtnCount, setLoadBtnCount] = useState(1);
  const itemsPerQuery = 10; // number of users per query calls (load more btn click)
  const api = useApi();
  const { userData } = useUserData();
  let apiUrl = '/api/feeds';
  useEffect(() => {
    loadPosts(loadBtnCount);
    if (loadBtnCount === 1) {
      setLoading(true);
    }
  }, []);
  const loadPosts = useCallback(
    (currentPage) => {
      if (currentPage > 1) setLoadingBtn(true);
      const pageScroll = window.pageYOffset; // get whole page offSetTop
      api
        .get(`${apiUrl}?items=${itemsPerQuery}&page=${currentPage}&order=desc`)
        .then((res) => {
          if (res.data.length !== 0) {
            // get total pages from response headers
            const totalPages = res?.headers['total-pages'];
            const newPosts = res.data;
            setPosts((prevPosts) => [...prevPosts, ...newPosts]);
            setLoading(false);
            setLoadingBtn(false);
            setLoadBtnCount(currentPage + 1);
            currentPage > 1 && window.scrollTo(0, pageScroll); // when clicking the load more button (not the first time it launch this function), force page to stay at pageScroll (else it would focus you at bottom of the list of posts)
            // loadBtnCount > 1 && window.scrollTo(0, pageScroll); // when clicking the load more button (not the first time it launch this function), force page to stay at pageScroll (else it would focus you at bottom of the list of posts)
            if (currentPage === parseInt(totalPages, 10) || (currentPage === 1 && newPosts.length === 0)) {
              setHideLoadBtn(true); // hide btn when the last "page" has been called (or if user feed is empty)
            } else setHideLoadBtn(false);
          } else {
            // if user has no post on their feed, show them the JOGL user feed by changing the apiUrl and triggering the function again
            setIsFeedEmpty(true);
            apiUrl = '/api/feeds/all';
            loadPosts(loadBtnCount);
          }
        })
        .catch((err) => {
          console.error(`Couldn't GET feeds with items=${itemsPerQuery} and page=${currentPage}`, err);
          setLoading(false);
          setLoadingBtn(false);
          setHideLoadBtn(true);
        });
    },
    // [api, loadBtnCount]
    [api]
  );

  const getFeedApi = () => {
    api
      .get(`${apiUrl}?items=10`)
      .then((res) => {
        setPosts(res.data);
      })
      .catch((err) => {
        console.error("Couldn't fetch feed items=10", err);
      });
  };

  const refresh = () => {
    // on refresh, reset feed and get only 5 latest posts
    getFeedApi();
  };

  const displayPosts = () => {
    if (posts.length !== 0) {
      return posts.map((post, index) => (
        <PostDisplay post={post} key={index} feedId={feedId} refresh={refresh} isHomeFeed user={userData} />
      ));
    }
    // return <FormattedMessage id="feed.empty" defaultMessage="Be the first to post something" />;
  };
  return (
    <div className="feed myFeed">
      <h3>
        <FormattedMessage id="feed.title" defaultMessage="Your Feed" />
      </h3>
      {displayCreate && userData && <PostCreate feedId={feedId} type="post" refresh={refresh} user={userData} />}
      <Loading active={loading} height="200px">
        {isFeedEmpty && (
          <Alert
            type="info"
            message={
              <FormattedMessage
                id="feed.noPosts"
                defaultMessage="You are seeing the general feed of JOGL. Start creating your personalized feed by following users, projects
          and more!"
              />
            }
          />
        )}
        {displayPosts()}
        {((posts.length > 10 && !hideLoadBtn) || !hideLoadBtn) && (
          <Button
            btnType="button"
            style={{ display: 'flex', margin: 'auto', justifyContent: 'center', alignItems: 'center' }}
            onClick={() => loadPosts(loadBtnCount)}
            disabled={loadingBtn}
          >
            {loadingBtn && (
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            <FormattedMessage id="general.load" defaultMessage="Load more" />
          </Button>
        )}
      </Loading>
    </div>
  );
}
