import { FC } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedMessage } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
interface Props {
  postId: number;
  type: string;
  commentId?: number;
  feedId?: number;
  origin: 'self' | 'other';
  refresh: () => void;
}

const PostDelete: FC<Props> = ({ postId, commentId, type, origin, feedId, refresh }) => {
  const api = useApi();
  const deletePost = () => {
    if (postId) {
      let apiUrl;
      if (origin === 'self') {
        apiUrl = type === 'post' ? `/api/posts/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
      } else {
        apiUrl = type === 'post' ? `/api/feeds/${feedId}/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
      }
      api.delete(apiUrl).then(() => {
        refresh();
      });
    }
  };
  return (
    <Box row alignItems="center" as="button" onClick={deletePost}>
      <FontAwesomeIcon icon="trash" className="postDelete" />
      <FormattedMessage id="feed.object.delete" defaultMessage="Delete" />
    </Box>
  );
};
export default PostDelete;
