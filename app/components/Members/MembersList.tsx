import React, { FC, useCallback, useEffect, useState } from 'react';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import { ItemType } from '~/types';
import Translate from '~/utils/Translate';
import Grid from '../Grid';
import Button from '../primitives/Button';
import MemberCard from './MemberCard';
import MembersModal from './MembersModal';

// import "./MembersList.scss";
interface Props {
  itemId: number;
  itemType: ItemType;
  isOwner: boolean;
}
const MembersList: FC<Props> = ({ itemId = 0, itemType, isOwner = false }) => {
  const [listMembers, setListMembers] = useState([]);
  const api = useApi();
  const { showModal, setIsOpen } = useModal();
  const getMembers = useCallback(() => {
    if (itemId) {
      api
        .get(`/api/${itemType}/${itemId}/members`)
        .then((res) => {
          // filter members by type
          const owners = res.data.members.filter((member) => member.owner);
          const admins = res.data.members.filter((member) => member.admin && !member.owner);
          const members = res.data.members.filter((member) => !member.owner && !member.admin && member.member);
          const pending = res.data.members.filter((member) => member.pending);
          // then group/order them, displaying owners first, then admins, then members, then pending members
          setListMembers([...owners, ...admins, ...members, ...pending]);
        })
        .catch((err) => {
          console.error(`Couldn't GET ${itemType} with itemId=${itemId}`, err);
        });
    }
  }, [api, itemId, itemType]);
  useEffect(() => {
    getMembers();
  }, [getMembers]);
  return (
    <div className="membersList">
      <div className="justify-content-end">
        <Button
          onClick={() => {
            showModal({
              children: <MembersModal itemType={itemType} itemId={itemId} />,
              title: 'Add new members',
              titleId: 'member.btnNewMembers',
              allowOverflow: true,
            });
          }}
        >
          <Translate id="member.btnNewMembers" defaultMessage="Add new members" />
        </Button>
      </div>
      {listMembers.length !== 0 ? (
        <Grid gridCols={1} display={['grid', 'inline-grid']} pt={8}>
          {listMembers
            // sort the array to have the pending members first
            ?.reduce((acc, element) => {
              if (element.pending) {
                return [element, ...acc];
              }
              return [...acc, element];
            }, [])
            // then map through it
            .map((member, i) => (
              <MemberCard
                itemId={itemId}
                itemType={itemType}
                member={member}
                key={i}
                callBack={getMembers}
                isOwner={isOwner}
              />
            ))}
        </Grid>
      ) : (
        <Loading />
      )}
    </div>
  );
};
export default MembersList;
