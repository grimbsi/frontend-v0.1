import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import ReactGA from 'react-ga';

export default function NeedBtnStatus({ need: propNeed }) {
  const [need, setNeed] = useState(propNeed);
  const [sending, setSending] = useState(false);
  const api = useApi();

  const changeStatus = () => {
    const newNeed = need;
    setSending(true);
    if (newNeed.status === undefined || newNeed.status === 'active') {
      newNeed.status = 'completed';
    } else {
      newNeed.status = 'active';
    }
    api
      .patch(`/api/needs/${newNeed.id}`, { need: newNeed })
      .then(() => {
        setNeed(newNeed);
        setSending(false);
        // send event to google analytics (only if user is closing the need)
        newNeed.status === 'completed' && ReactGA.event({ category: 'Need', action: 'close', label: `${newNeed.id}` });
        document.querySelector('.btn.cancel').click();
      })
      .catch(() => setSending(false));
  };
  if (need && need.is_owner) {
    return (
      <button className="btn btn-warning btnStatus" onClick={changeStatus} type="button" disabled={sending}>
        {need.status === 'completed' && sending && (
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
            {' '}
            &nbsp;
          </span>
        )}
        {need.status === 'completed' && !sending && <FormattedMessage id="need.card.reopen" defaultMessage="Reopen" />}
        {need.status !== 'completed' && sending && (
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
            {' '}
            &nbsp;
          </span>
        )}
        {need.status !== 'completed' && !sending && <FormattedMessage id="need.card.close" defaultMessage="Close" />}
      </button>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
}
