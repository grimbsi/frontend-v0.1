import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { layout } from 'styled-system';
import useChallenges from '~/hooks/useChallenges';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Need } from '~/types';
import styled from '~/utils/styled';
import Box from '../Box';
import Filters from '../Filters';
import Grid from '../Grid';
import NeedCard from '../Need/NeedCard';
import A from '../primitives/A';
import P from '../primitives/P';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';

const ProgramNeeds = ({ programId }) => {
  const [needsEndpoints, setNeedsEndpoint] = useState(`/api/programs/${programId}/needs`);
  const { dataChallenges, challengesError } = useChallenges('programs', programId);
  const { data: dataNeeds } = useGet<{ needs: Need[] }>(needsEndpoints);
  const [selectedChallengeFilterId, setSelectedChallengeFilterId] = useState(undefined);
  const { userData } = useUserData();
  const onFilterChange = (e) => {
    const id = e.target.name;
    if (id) {
      setSelectedChallengeFilterId(Number(id));
      setNeedsEndpoint(`/api/challenges/${id}/needs`);
    } else {
      setSelectedChallengeFilterId(undefined);
      setNeedsEndpoint(`/api/programs/${programId}/needs`);
    }
  };

  const OverflowGradient = styled.div`
    ${layout};
    width: 3rem;
    height: 100%;
    position: absolute;
    right: 0;
    background: ${(p) =>
      `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
  `;
  return (
    <div>
      <Box>
        <P>
          <FormattedMessage
            id="program.needs.text"
            defaultMessage="To participate to this program, participate to as many of its needs. Check out the needs already submitted, contribute to them or create your own!"
          />
        </P>
        {!userData && ( // if user is not connected
          <A href="/header.signIn" as="/signin">
            <FormattedMessage
              id="header.signIn"
              defaultMessage={'Sign in {toFillNeed}'}
              values={{
                toFillNeed: <FormattedMessage id="program.signinCta.need" defaultMessage="to fill a need" />,
              }}
            />
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel={{ id: 'challenge.list.all.title', defaultMessage: 'All challenges' }}
            content={dataChallenges?.map(({ title, id }) => ({
              title,
              id,
            }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!challengesError}
            errorMessage="Could not get challenges filters"
            selectedId={selectedChallengeFilterId}
          />
        </Box>
        {!dataNeeds ? (
          <Loading />
        ) : dataNeeds?.needs.length === 0 ? (
          <NoResults type="need" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
            {dataNeeds.needs.map((need, i) => (
              <NeedCard
                key={i}
                title={need.title}
                project={need.project}
                skills={need.skills}
                resources={need.ressources}
                hasSaved={need.has_saved}
                id={need.id}
                postsCount={need.posts_count}
                membersCount={need.members_count}
                publishedDate={need.created_at}
                dueDate={need.end_date}
                status={need.status}
              />
            ))}
          </Grid>
        )}
      </Box>
    </div>
  );
};

export default ProgramNeeds;
