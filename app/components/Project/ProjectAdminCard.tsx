import Alert from '~/components/Tools/Alert';
import { useState, FC } from 'react';
import { useApi } from '~/contexts/apiContext';
import { Project } from '~/types';
import Box from '../Box';
import A from '../primitives/A';
import { FormattedMessage } from 'react-intl';
import Button from '../primitives/Button';

interface Props {
  project: Project;
  callBack: () => void;
}

const ProjectAdminCard: FC<Props> = ({ project, callBack }) => {
  const [sending, setSending] = useState('');
  const [error, setError] = useState('');
  const api = useApi();

  const changeStatusProject = (newStatus) => {
    setSending(newStatus);
    api
      .post(`/api/challenges/${project.challenge_id}/projects/${project.id}`, { status: newStatus })
      .then(() => {
        setSending('');
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't POST challenge with challengeId=${project.challenge_id}`, err);
        setSending('');
        setError(<FormattedMessage id="err-" defaultMessage="An error has occured" />);
      });
  };

  const removeProject = () => {
    setSending('remove');
    api
      .delete(`/api/challenges/${project.challenge_id}/projects/${project.id}`)
      .then(() => {
        setSending('');
        callBack();
      })
      .catch((err) => {
        console.error(`Couldn't DELETE challenge with challengeId=${project.challenge_id}`, err);
        setSending('');
        setError(<FormattedMessage id="err-" defaultMessage="An error has occured" />);
      });
  };

  if (project !== undefined) {
    let imgTodisplay = '/images/default/default-project.jpg';
    if (project.banner_url_sm) {
      imgTodisplay = project.banner_url_sm;
    }

    const bgBanner = {
      backgroundImage: `url(${imgTodisplay})`,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      border: '1px solid #ced4da',
      borderRadius: '50%',
      height: '50px',
      width: '50px',
    };

    return (
      <div>
        <Box row justifyContent="space-between" key={project.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgBanner} />
            </div>
            <Box>
              <A href="/project/[id]/[[...index]]" as={`/project/${project.id}/${project.short_title}`}>
                {project.title}
                <br />
              </A>
              <Box fontSize="93%">
                <FormattedMessage id="attach.members" defaultMessage="Members : " />
                {project.members_count}
              </Box>
            </Box>
          </Box>
          <Box row alignItems="center" ml={3}>
            {project.challenge_status === 'pending' ? ( // if project status is pending, display the accept/reject buttons
              <Box flexDirection={['column', 'row']}>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'accepted'}
                  onClick={() => changeStatusProject('accepted')}
                >
                  {sending === 'accepted' && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.accept" defaultMessage="Accept" />
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'remove'}
                  onClick={() => removeProject()}
                >
                  {sending === 'remove' && (
                    <>
                      <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="general.reject" defaultMessage="Reject" />
                </button>
              </Box>
            ) : (
              // else display the remove button
              <Button btnType="danger" disabled={sending === 'remove'} onClick={() => removeProject()}>
                {sending === 'remove' && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="attach.remove" defaultMessage="remove" />
              </Button>
            )}
            {error !== '' && <Alert type="danger" message={error} />}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProjectAdminCard;
