import Link from 'next/link';
import React, { FC } from 'react';
import { WidthProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource } from '~/types';
import { textWithPlural } from '~/utils/managePlurals';
import Chips from '../Chip/Chips';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  clapsCount: number;
  members_count: number;
  needs_count: number;
  has_saved: boolean;
  banner_url: string;
  width?: WidthProps['width'];
  cardFormat?: string;
  source?: DataSource;
  skills?: any;
}
const ProjectCard: FC<Props> = ({
  id,
  title,
  shortTitle = undefined,
  short_description,
  clapsCount,
  members_count,
  needs_count,
  has_saved,
  banner_url = '/images/default/default-project.jpg',
  width,
  cardFormat,
  source,
  skills,
}) => {
  const projectUrl = { href: `/project/[id]/[[...index]]`, as: `/project/${id}/${shortTitle}` };
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  return (
    <Card imgUrl={banner_url} isImgSmall={cardFormat === 'compact'} imgLinkObject={projectUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={projectUrl.href} as={projectUrl.as} passHref>
          <Title pr={2}>
            <H2 fontSize={TitleFontSize}>{title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="projects" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <Box flex="1">{short_description}</Box>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            as: `/search/projects/?refinementList[skills][0]=${skill}`,
            href: `/search/[active-index]/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={{ href: '/project/[id]/[[...index]]', as: `/project/${id}/${shortTitle}` }}
          // color={theme.colors.primary}
          color="#F2F4F8"
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2}>
          <CardData value={members_count} title={textWithPlural('member', members_count)} />
          <CardData value={needs_count} title={textWithPlural('need', needs_count)} />
          <CardData value={clapsCount} title={textWithPlural('clap', clapsCount)} />
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default ProjectCard;
