import React, { useState, useMemo } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import Alert from '../Tools/Alert';
import Button from '../primitives/Button';
import Box from '../Box';
import Loading from '../Tools/Loading';

interface PropsModal {
  alreadyPresentProjects: any[];
  challengeId: number;
  programId: number;
  isMember: number;
  mutateProjects: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
  closeModal: () => void;
}
export const ProjectLinkModal: FC<PropsModal> = ({
  alreadyPresentProjects,
  challengeId,
  programId,
  isMember,
  mutateProjects,
  closeModal,
}) => {
  const { data: dataProjectsMine, error } = useGet('/api/projects/mine');
  const [selectedProject, setSelectedProject] = useState();
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();
  const { formatMessage } = useIntl();

  // Filter the projects that are already in this challenge so you don't add it twice!
  const filteredProjects = useMemo(() => {
    if (dataProjectsMine) {
      return dataProjectsMine.filter((projectMine) => {
        // Check if my project is found in alreadyPresentProjects
        const isMyProjectAlreadyPresent = alreadyPresentProjects
          // filter to not add the pending ones so they appear in the other list, marked as pending
          .filter(({ challenge_status }) => challenge_status !== 'pending')
          .find((alreadyPresentProject) => {
            return alreadyPresentProject.id === projectMine.id;
          });
        // We keep only the ones that are not present
        return !isMyProjectAlreadyPresent;
      });
    }
    return undefined;
  }, [dataProjectsMine, alreadyPresentProjects]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this project to the challenge then mutate the cache of the projects from the parent prop
    if ((selectedProject as { id: number })?.id) {
      await api
        .put(`/api/challenges/${challengeId}/projects/${(selectedProject as { id: number }).id}`)
        .catch(() =>
          console.error(`Could not PUT/link challengeId=${challengeId} with project projectId=${selectedProject.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      mutateProjects({ projects: [...alreadyPresentProjects, selectedProject] });
      !isMember && api.put(`/api/challenges/${challengeId}/join`); // join the challenge if user is not member already
      api.put(`/api/challenges/${challengeId}/follow`); // then follow it
      api.put(`/api/programs/${programId}/follow`); // and follow its program
      setTimeout(() => {
        // close modal after 3.5sec
        closeModal();
      }, 3500);
    }
  };

  const onProjectSelect = (e) => {
    setSelectedProject(filteredProjects.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      <Box mb={[7, 4]}>
        <a href="/project/create" target="_blank" style={{ right: '1rem', position: 'absolute' }}>
          <FormattedMessage id="projects.searchBar.btnAddProject" defaultMessage="Add a project" />
        </a>
      </Box>
      {!filteredProjects ? (
        <Loading />
      ) : filteredProjects?.length > 0 && dataProjectsMine?.filter((project) => project.is_admin).length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredProjects
            .filter((project) => project.is_admin)
            .map((project, index) => {
              const projectCurrentChallengeInfo = project.challenges.filter(
                (challenge) => challenge.id === challengeId
              );
              const isPending = // check if project's submission to the challenge is pending, and then disable to radio and add a text
                projectCurrentChallengeInfo.length !== 0 && projectCurrentChallengeInfo[0].project_status === 'pending';
              return (
                <div className={`form-check ${isPending && 'disabled'}`} key={index} style={{ height: '50px' }}>
                  <input
                    type="radio"
                    className="form-check-input"
                    name="exampleRadios"
                    id={`project-${index}`}
                    value={project.id}
                    onChange={onProjectSelect}
                    disabled={isPending}
                  />
                  <label className="form-check-label" htmlFor={`project-${index}`}>
                    {project.title}
                    {isPending &&
                      ` (${formatMessage({
                        id: 'challenge.acceptState.pendingApproval',
                        defaultMessage: 'Pending approval',
                      })})`}
                  </label>
                </div>
              );
            })}
          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} mb={2}>
              <>
                {sending && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="attach.project.btnSend" defaultMessage="Submit this project" />
              </>
            </Button>
            {requestSent && (
              <Alert
                type="success"
                message={
                  <FormattedMessage
                    id="attach.project.success"
                    defaultMessage="The project has been sent. He will be examined by the challenge team to validate his commitment to the challenge."
                  />
                }
              />
            )}
          </div>
        </form>
      ) : (
        <div className="noProject" style={{ textAlign: 'center' }}>
          <FormattedMessage id="attach.project.noProject" defaultMessage="You do not have a project to add" />
        </div>
      )}
    </div>
  );
};
