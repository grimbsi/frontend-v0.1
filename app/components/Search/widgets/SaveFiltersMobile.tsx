import { connectStats } from 'react-instantsearch-dom';
import { FormattedMessage } from 'react-intl';
import { formatNumber } from '~/utils/utils';

const SaveFiltersMobile = ({ nbHits, onClick }) => (
  <button className="button button-primary" onClick={onClick}>
    <FormattedMessage
      id="algolia.seeResults"
      defaultMessage={'See {number} results'}
      values={{ number: formatNumber(nbHits) }}
    />
  </button>
);

export default connectStats(SaveFiltersMobile);
