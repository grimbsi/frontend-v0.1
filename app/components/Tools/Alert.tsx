import { FC, ReactNode } from 'react';
interface Props {
  message: string | ReactNode;
  type: string;
  postId?: number;
  commentId?: number;
  id?: string;
}
const Alert: FC<Props> = ({ message, type, id, postId, commentId }) => {
  const postOrComment = postId ? 'post' : commentId ? 'comment' : '';
  const objectId = postId || commentId;
  return (
    <div className={`w-100 alert alert-${type} alert-${objectId} ${postOrComment}`} id={id} role="alert">
      {message}
    </div>
  );
};

export default Alert;
