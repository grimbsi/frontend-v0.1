import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FC, ReactNode, useCallback, useEffect, useState, Fragment } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useApi } from '~/contexts/apiContext';
import useUserData from '~/hooks/useUserData';
import { DataSource, ItemType } from '~/types';
import styled from '~/utils/styled';
import Button from '../primitives/Button';
import ReactTooltip from 'react-tooltip';
import Box from '../Box';
import ReactGA from 'react-ga';

interface Props {
  btnType?: string;
  displayButton?: boolean;
  followState: boolean;
  itemType: ItemType;
  itemId: number;
  textFollow?: string | ReactNode;
  textUnfollow?: string | ReactNode;
  source?: DataSource;
  width?: any;
}
const BtnFollow: FC<Props> = ({
  btnType = 'primary',
  displayButton = false,
  followState: followStateProp = false,
  itemType = undefined,
  itemId = undefined,
  textFollow = <FormattedMessage id="general.follow" defaultMessage="Follow" />,
  textUnfollow = <FormattedMessage id="general.unfollow" defaultMessage="Unfollow" />,
  source = 'api',
  width,
}) => {
  const [followState, setFollowState] = useState(followStateProp);
  const [sending, setSending] = useState(false);
  const [icon, setIcon] = useState(['far', 'bell-slash']);
  const api = useApi();
  const { formatMessage } = useIntl();
  const { userData, userDataError } = useUserData();
  const getApiFollowState = useCallback(() => {
    if (userData) {
      api
        .get(`/api/${itemType}/${itemId}/follow`)
        .then((res) => {
          setFollowState(res.data.has_followed);
        })
        .catch((err) => {
          console.error(`Couldn't GET /api/${itemType}/${itemId}/follow`, err);
        });
    }
    setFollowState(false);
  }, [api, itemId, itemType, userData]);

  useEffect(() => {
    // change icon when followState changes
    setIcon(followState ? ['fas', 'bell'] : ['far', 'bell-slash']);
  }, [followState]);

  useEffect(() => {
    // if data also comes from algolia (and not only api), get follow state from api (because it's not available in algolia)
    if (source === 'algolia') getApiFollowState();
  }, [getApiFollowState, source]);

  const changeStateFollow = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      // send event to google analytics
      if (followState === false)
        // for some reason, there is an error when tracking unfollow
        ReactGA.event({ category: 'Button', action: followState, label: `${itemType} ${itemId}` });
      const action = followState ? 'unfollow' : 'follow';
      if (action === 'follow') {
        // if action is follow
        api
          .put(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't PUT /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      } else {
        // else delete follow
        api
          .delete(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't DELETE /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      }
    }
  };
  const Bell = styled(FontAwesomeIcon)`
    cursor: pointer;
    font-size: 1.5rem !important;
    color: #da9c00;
    opacity: 0.8;
  `;
  const text = followState ? textUnfollow : textFollow;
  const action = followState ? 'unfollow' : 'follow';
  // if displayButton if not set to true, display icon by default
  if (!displayButton)
    return (
      <>
        {sending ? (
          <span>
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
            &nbsp;
          </span>
        ) : (
          <>
            <Box data-tip={formatMessage({ id: `general.${action}`, defaultMessage: action })} data-for="follow">
              <Bell
                icon={icon}
                onClick={changeStateFollow}
                onKeyUp={changeStateFollow}
                onMouseEnter={() => {
                  // change icon depending on follow state
                  setIcon(followState ? ['far', 'bell-slash'] : ['fas', 'bell']);
                }}
                onMouseLeave={() => {
                  // change icon depending on follow state
                  setIcon(followState ? ['fas', 'bell'] : ['far', 'bell-slash']);
                }}
                tabIndex={0}
              />
              <ReactTooltip id="follow" delayHide={300} effect="solid" />
            </Box>
          </>
        )}
      </>
    );
  // else display full button
  return (
    <Button onClick={changeStateFollow} onKeyUp={changeStateFollow} btnType={btnType} disabled={sending} width={width}>
      {sending ? (
        <span>
          <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
          &nbsp; {text}
        </span>
      ) : (
        text
      )}
    </Button>
  );
};
export default BtnFollow;
