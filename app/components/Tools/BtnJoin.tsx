// ! This component is too complicated for a simple join button, it should be a "controlled component"
// ? Maybe this component shouldn't exist
// TODO: change props to it only : <BtnJoin isJoined={true} onClick={() => parentFunc()} textJoin={} textUnjoin={} />
// So remove all the API requests from the component
// Modal should be called from the parent with the onClick
import { Component, ReactNode } from 'react';
import { FormattedMessage } from 'react-intl';
import { ApiContext } from '~/contexts/apiContext';
import { ItemType } from '~/types';
import Button from '../primitives/Button';

interface Props {
  type: string;
  itemType: ItemType;
  itemId: number;
  joinState: boolean | 'pending';
  sending: boolean;
  textJoin: string | ReactNode;
  textUnjoin: string | ReactNode;
  isPrivate?: boolean;
  onJoin?: () => void;
}
export default class BtnJoin extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      joinState: this.props.joinState,
    };
  }

  static get defaultProps() {
    return {
      type: 'primary',
      itemType: undefined,
      itemId: undefined,
      joinState: false,
      sending: false,
      textJoin: <FormattedMessage id="general.join" defaultMessage="Join team" />,
      textUnjoin: <FormattedMessage id="general.leave" defaultMessage="Leave team" />,
      textPending: <FormattedMessage id="general.pending" defaultMessage="Pending" />,
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { joinState } = nextProps;
    this.setState({ joinState });
  }

  changeStateJoin(action, joinState) {
    const { itemId, itemType, isPrivate, onJoin } = this.props;
    const api = this.context;
    if (!itemId || !itemType) {
    } else {
      this.setState({ sending: true });
      api
        .put(`/api/${itemType}/${itemId}/${action}`)
        .then(() => {
          this.setState({
            // if object is private, set joinState as pending on "join" action, or false if "leave".
            // if not private, set to opposite current joinState
            joinState: isPrivate ? (action === 'join' ? 'pending' : false) : !joinState,
            sending: false,
          });
          api.put(`/api/${itemType}/${itemId}/follow`); // follow the object after having joined it
          // if object is not private, refresh page onJoin
          !isPrivate && action === 'join' && onJoin();
          // send event to google analytics
          ReactGA.event({ category: 'Button', action: action, label: `${itemType} ${itemId}` });
        })
        .catch((err) => {
          console.error(`Couldn't PUT ${itemType} with itemId=${itemId} and action=${action}`, err);
          this.setState({ sending: false });
        });
    }
  }

  render() {
    const { joinState, sending } = this.state;
    const { type, textJoin, textUnjoin, textPending } = this.props;
    const action = joinState ? 'leave' : 'join';
    // change text of button depending on the join state
    const btnText = joinState === true ? textUnjoin : joinState === 'pending' ? textPending : textJoin;
    return (
      <Button onClick={() => this.changeStateJoin(action, joinState)} type={type} disabled={sending}>
        {sending ? (
          <span>
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
            &nbsp; {btnText}
          </span>
        ) : (
          btnText
        )}
      </Button>
    );
  }
}
BtnJoin.contextType = ApiContext;
