import { useState, FC } from 'react';
import { useIntl } from 'react-intl';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import Button from '../primitives/Button';
import ReactGA from 'react-ga';

interface PropsModal {
  itemId: number;
  closeModal: () => void;
}
export const ContactForm: FC<PropsModal> = ({ itemId, closeModal }) => {
  const [object, setObject] = useState('');
  const [message, setMessage] = useState('');
  const [showConf, setShowConf] = useState(false);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const intl = useIntl();
  const handleChange = (event) => {
    if (event.target.name === 'object') {
      setObject(event.target.value);
    } else if (event.target.name === 'message') {
      setMessage(event.target.value);
    } else {
      console.warn(`${event.target.name} is not handled`);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setSending(true);
    const param = {
      object,
      content: message,
    };
    setShowConf(true);
    setSending(false);
    api
      .post(`/api/users/${itemId}/send_email`, param)
      .then(() => {
        setShowConf(true);
        setSending(false);
        setTimeout(() => {
          setShowConf(false);
          setMessage('');
          closeModal(); // close modal
        }, 2500);
        // send event to google analytics
        ReactGA.event({ category: 'Contact', action: 'contact user', label: itemId });
      })
      .catch((err) => {
        console.error(err);
      });
  };
  return (
    <form className="contactForm" onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor={`recipient-name-${itemId}`} className="col-form-label">
          {intl.formatMessage({ id: 'user.contactModal.subject', defaultMessage: 'Subject*' })}
        </label>
        <input
          type="text"
          className="form-control object"
          id={`recipient-name-${itemId}`}
          name="object"
          onChange={handleChange}
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor={`message-text-${itemId}`} className="col-form-label">
          {intl.formatMessage({ id: 'user.contactModal.message', defaultMessage: 'Message*' })}
        </label>
        <textarea
          className="form-control message"
          style={{ minHeight: '200px' }}
          id={`message-text-${itemId}`}
          name="message"
          onChange={handleChange}
          required
        />
      </div>
      {!showConf ? ( // show send button if not showing the confirmation message
        // disable button ono sending, or if object or message is empty
        <Button type="submit" disabled={sending || !message || !object}>
          <>
            {sending && ( // show spinner on submit
              <>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                &nbsp;
              </>
            )}
            {intl.formatMessage({ id: 'user.contactModal.send', defaultMessage: 'Send' })}
          </>
        </Button>
      ) : (
        // on submit, replace submit button by this message
        <Alert
          type="success"
          message={intl.formatMessage({
            id: 'user.contactModal.success',
            defaultMessage: 'Your message was sent!',
          })}
        />
      )}
    </form>
  );
};
