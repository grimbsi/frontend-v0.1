import { Component } from 'react';
// import "./InfoDefaultComponent.scss";
import TitleInfo from '~/components/Tools/TitleInfo';

export default class InfoDefaultComponent extends Component {
  static get defaultProps() {
    return {
      title: 'Title',
      content: '',
      prepend: '',
      containsHtml: false,
    };
  }

  render() {
    const { title, content, prepend, containsHtml } = this.props;

    let titleinfo = '';

    if (title) {
      titleinfo = <TitleInfo title={title} />;
    }

    if (content) {
      return (
        <div className="infoDefault">
          {titleinfo}
          <div className="content">
            {!containsHtml ? prepend + content : <div dangerouslySetInnerHTML={{ __html: content }} />}
          </div>
        </div>
      );
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
}
