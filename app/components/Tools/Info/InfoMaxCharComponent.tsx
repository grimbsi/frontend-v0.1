import { Component } from 'react';
import { FormattedMessage } from 'react-intl';

export default class InfoMaxCharComponent extends Component {
  render() {
    const { content, maxChar } = this.props;
    if (maxChar !== undefined) {
      let actualSize = 0;
      if (content) {
        actualSize = content.length;
      }
      let style = { textAlign: 'right', color: '#566573' };
      if (actualSize > maxChar) {
        style = { textAlign: 'right', color: 'red' };
        return (
          <div className="maxChar" style={style}>
            {actualSize}/{maxChar} max.
          </div>
        );
      }
      if (actualSize > maxChar * 0.8) {
        style = { textAlign: 'right', color: '#D4AC0D' };
        return (
          <div className="maxChar" style={style}>
            {actualSize}/{maxChar} max.
          </div>
        );
      }
      if (actualSize > maxChar * 0.7) {
        return (
          <div className="maxChar" style={style}>
            {maxChar} <FormattedMessage id="form.maxChar" defaultMessage="characters max." />
          </div>
        );
      }
      // eslint-disable-next-line @rushstack/no-null
      return null;
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
}
