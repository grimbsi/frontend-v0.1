import React, { FC, useMemo } from 'react';
import ProjectList from '~/components/Project/ProjectList';
import Loading from '~/components/Tools/Loading';
import useGet from '~/hooks/useGet';
import { ItemType } from '~/types';

interface Props {
  itemId: number;
  itemType: ItemType;
}

const ListProjectsAttached: FC<Props> = ({ itemId, itemType }) => {
  const { data, loading } = useGet(`/api/${itemType}/${itemId}/projects`);
  const filteredProjects = useMemo(() => {
    if (data) {
      return data.projects.filter(({ challenge_status }) => challenge_status !== 'pending');
    }
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }, [data]);
  return (
    <div className="listProjectsAttached">
      <Loading active={loading}>
        {!loading && filteredProjects && <ProjectList listProjects={filteredProjects} />}
      </Loading>
    </div>
  );
};
export default ListProjectsAttached;
