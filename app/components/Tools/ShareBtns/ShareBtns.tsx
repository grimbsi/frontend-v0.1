import { FormattedMessage, useIntl } from 'react-intl';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRouter } from 'next/router';
import { useModal } from '~/contexts/modalContext';
import { copyLink } from '~/utils/utils';
import Alert from '../Alert';
import ReactTooltip from 'react-tooltip';
import ReactGA from 'react-ga';

export default function ShareBtns({ type = '', needId = '', postId = '' }) {
  const router = useRouter();
  const intl = useIntl();
  const { showModal, setIsOpen } = useModal();
  let typeIntl = undefined;
  switch (type) {
    case 'project':
      typeIntl = { id: 'project', defaultMessage: 'Project' };
      break;
    case 'challenge':
      typeIntl = { id: 'challenge', defaultMessage: 'Challenge' };
      break;
    case 'community':
      typeIntl = { id: 'community', defaultMessage: 'Group' };
      break;
    case 'program':
      typeIntl = { id: 'program', defaultMessage: 'Program' };
      break;
    case 'need':
      typeIntl = { id: 'need', defaultMessage: 'Need' };
      break;
    case 'post':
      typeIntl = { id: 'post', defaultMessage: 'Post' };
      break;
    default:
      typeIntl = { id: 'project', defaultMessage: 'Project' };
  }

  const onShareButtonClick = (e) => {
    if (navigator.share) {
      // make urlPath the router asPath, except if we are sharing need or posts, in which case the url is the one from the single need or post page
      const urlPath = type === 'need' ? `/need/${needId}` : type === 'post' ? `/post/${postId}` : router.asPath;
      navigator
        .share({
          title: `${type}`,
          text: `Check out this ${type} on the JOGL platform: `,
          url: process.env.ADDRESS_FRONT + urlPath,
        })
        .catch(console.error);
    } else {
      if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
        // if function is launched via keypress, execute only if it's the 'enter' key
        showModal({
          children: <Modal type={type} needId={needId} postId={postId} hideModal={() => setIsOpen(false)} />,
          title: 'Share this {type}',
          titleId: 'general.shareBtns.modalTitle',
          values: { type: intl.formatMessage(typeIntl) },
        });
      }
    }
    // send event to google analytics
    ReactGA.event({ category: 'Button', action: 'Share', label: type });
  };

  return (
    <div className="shareBtns">
      <button
        className={
          type === 'post' || type === 'need'
            ? 'btn-postcard'
            : type === 'program' || type === 'challenge'
            ? 'share-button-bg special'
            : 'share-button-bg'
        }
        type="button"
        title={`Share this ${type}`}
        onClick={onShareButtonClick}
        onKeyUp={onShareButtonClick}
        tabIndex={0}
        // data-tip={intl.formatMessage({ id: 'general.shareBtns.modalTitle', defaultMessage: 'Share' })}
        data-tip={intl.formatMessage(
          {
            id: 'general.shareBtns.modalTitle',
            defaultMessage: 'Share this {type}',
          },
          { type: intl.formatMessage(typeIntl) }
        )}
        // data-tip={`Share this ${type}`}
        data-for="share"
        // show/hide tooltip on element focus/blur
        onFocus={(e) => ReactTooltip.show(e.target)}
        onBlur={(e) => ReactTooltip.hide(e.target)}
      >
        {type === 'post' || type === 'need' || type === 'program' || type === 'challenge' ? (
          <FontAwesomeIcon icon="share" size="lg" />
        ) : (
          <FontAwesomeIcon icon="share-alt" />
        )}
        {type !== 'program' && type !== 'challenge' && (
          <span>{intl.formatMessage({ id: 'general.shareBtns.btnTitle', defaultMessage: 'Share' })}</span>
        )}
      </button>
      <ReactTooltip id="share" delayHide={300} effect="solid" type="dark" />
    </div>
  );
}

const Modal = ({ type, needId, postId }) => {
  const router = useRouter();
  // set objectId depending on type (used to copy link)
  const objectId =
    type === 'post'
      ? postId
      : type === 'need'
      ? needId
      : type === 'challenge' || type === 'program'
      ? router.query.short_title
      : router.query.id;
  const intl = useIntl();
  let currentUrl = encodeURIComponent(process.env.ADDRESS_FRONT + router.asPath);
  const openWindow = (socialMedia) => {
    // send event to google analytics
    ReactGA.event({ category: 'Button', action: `Desktop share via ${socialMedia}`, label: type });
    if (type === 'need') currentUrl = `${process.env.ADDRESS_FRONT}/need/${needId}`;
    if (type === 'post') currentUrl = `${process.env.ADDRESS_FRONT}/post/${postId}`;
    let shareUrl;
    if (socialMedia === 'facebook') shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`;
    if (socialMedia === 'twitter')
      shareUrl = `https://twitter.com/intent/tweet/?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&hashtags=JOGL&url=${currentUrl}`;
    if (socialMedia === 'linkedin')
      shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}&title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&amp;&source=${currentUrl}`;
    if (socialMedia === 'reddit')
      shareUrl = `https://reddit.com/submit/?url=${currentUrl}&resubmit=true&amp;title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;`;
    if (socialMedia === 'whatsapp')
      shareUrl = `https://api.whatsapp.com/send?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;${currentUrl}`;
    if (socialMedia === 'telegram')
      shareUrl = `https://telegram.me/share/url?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;url=${currentUrl}`;
    window.open(shareUrl, 'share-dialog', 'width=626,height=436');
  };
  return (
    <div className="share-dialog is-open">
      <div className="targets">
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('facebook')}
          aria-label="Share on Facebook"
        >
          <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'facebook']} /> Facebook
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('twitter')}
          aria-label="Share on Twitter"
        >
          <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'twitter']} /> Twitter
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('linkedin')}
          aria-label="Share on LinkedIn"
        >
          <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'linkedin']} /> LinkedIn
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('reddit')}
          aria-label="Share on Reddit"
        >
          <div className="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'reddit-alien']} /> Reddit
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('whatsapp')}
          aria-label="Share on WhatsApp"
        >
          <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'whatsapp']} /> Whatsapp
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href="#"
          onClick={() => openWindow('telegram')}
          aria-label="Share on Telegram"
        >
          <div className="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--large">
            <FontAwesomeIcon icon={['fab', 'telegram-plane']} /> Telegram
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform;&body=${currentUrl}`}
          target="_self"
          rel="noopener"
          aria-label="Share by E-Mail"
        >
          <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
            <FontAwesomeIcon icon="envelope" /> Email
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => copyLink(objectId, type, undefined)}
          rel="noopener"
          aria-label="Copy link"
        >
          <div className="resp-sharing-button resp-sharing-button--link resp-sharing-button--large">
            <FontAwesomeIcon icon="link" />{' '}
            {intl.formatMessage({ id: 'general.copyLink', defaultMessage: 'Copy link' })}
          </div>
        </button>
      </div>
      <Alert
        id="copyConfirmation"
        postId={objectId}
        type="success"
        message={<FormattedMessage id="general.copyLink.conf" defaultMessage="The link has been copied" />}
      />
    </div>
  );
};
