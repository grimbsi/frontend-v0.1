import React, { useContext, FC, useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import { Menu, MenuItem, MenuLink } from '@reach/menu-button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { UserContext } from '~/contexts/UserProvider';
import { BtnUserMenu, Container } from './UserMenu.styles';
import { DropDownMenu } from '~/components/Header/Header.styles';
import Loading from '../Tools/Loading';
import useUserData from '~/hooks/useUserData';
import UserShowObjects from './UserShowObjects';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import ProjectList from '../Project/ProjectList';
import CommunityList from '../Community/CommunityList';
import styled from '~/utils/styled';
import Box from '../Box';
import Grid from '../Grid';
import ChallengeCard from '../Challenge/ChallengeCard';
import NoResults from '../Tools/NoResults';
import ProgramCard from '../Program/ProgramCard';
interface Props {}
const UserMenu: FC<Props> = () => {
  const userContext = useContext(UserContext);
  const { userData } = useUserData();
  const { showModal } = useModal();
  if (userData) {
    return (
      <Container>
        <Menu>
          <BtnUserMenu style={{ backgroundImage: `url(${userData.logo_url_sm})` }} />
          <UserDropDownMenu>
            <Link href="/user/[id]/[[...index]]" as={`/user/${userData.id}/${userData.nickname}`} passHref>
              <MenuLink to={`/user/${userData.id}/${userData.nickname}`}>
                <FontAwesomeIcon icon="user-circle" />
                <FormattedMessage id="menu.profile.profile" defaultMessage="Profile" />
              </MenuLink>
            </Link>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <UsersObjects />,
                  title: 'My objects',
                  titleId: 'user.objects',
                  maxWidth: '70rem',
                });
              }}
            >
              <FontAwesomeIcon icon="list" />
              <FormattedMessage id="user.objects" defaultMessage="My objects" />
            </MenuItem>
            <MenuItem
              onSelect={() => {
                showModal({
                  children: <SavedObjectsModal />,
                  title: 'My saved objects',
                  titleId: 'user.profile.saved_objects',
                  maxWidth: '70rem',
                });
              }}
            >
              <FontAwesomeIcon icon="bookmark" />
              <FormattedMessage id="user.profile.saved" defaultMessage="Saved" />
            </MenuItem>
            <Link href="/user/[id]/settings" as={`/user/${userData.id}/settings`} passHref>
              <MenuLink to={`/user/${userData.id}/settings`}>
                <FontAwesomeIcon icon="cog" />
                <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
              </MenuLink>
            </Link>
            <MenuItem
              onSelect={() => {
                window.location.href = 'mailto:support@jogl.io';
              }}
            >
              <FontAwesomeIcon icon="question-circle" />
              <FormattedMessage id="menu.profile.help" defaultMessage="Help" />
            </MenuItem>
            <MenuItem onSelect={() => userContext.logout('/')}>
              <FontAwesomeIcon icon="sign-out-alt" />
              <FormattedMessage id="menu.profile.logout" defaultMessage="Logout" />
            </MenuItem>
          </UserDropDownMenu>
        </Menu>
      </Container>
    );
  } else {
    return <Loading />;
  }
};

const UserDropDownMenu = styled(DropDownMenu)`
  a,
  div {
    padding: 6px 18px;
    @media (max-width: ${(p) => p.theme.breakpoints.lg}) {
      padding: 10px 18px;
    }
  }
  div[role='none'] {
    padding: 0 !important;
  }
`;

const SavedObjectsModal = () => {
  const { data: savedObjects } = useGet(`/api/users/saved_objects`);
  return <UserShowObjects list={savedObjects} type="saved" />;
};

const Nav = styled.nav`
  margin-top: 0px;
  justify-content: flex-start;
  border-bottom: 1px solid #dee2e6;
  position: sticky;
  top: -1rem;
  background: white;
  margin-bottom: 1rem;
  z-index: 1;
  .nav-item {
    color: #5c5d5d;
    border: none !important;
    border-bottom: 2px solid transparent !important;
    font-size: 18px;
    font-weight: 500;
    width: fit-content;
    margin-bottom: 0;
    &.active,
    &:hover {
      border-bottom-color: #2987cd !important;
    }
  }
`;

const UsersObjects = () => {
  const { data: userProjects } = useGet(`/api/projects/mine`);
  const { data: userGroups } = useGet(`/api/communities/mine`);
  const { data: userChallenges } = useGet(`/api/challenges/mine`);
  const { data: userPrograms } = useGet(`/api/programs/mine`);
  const projectsAdmin = userProjects?.filter(({ is_admin }) => is_admin);
  const groupsAdmin = userGroups?.filter(({ is_admin }) => is_admin);
  const challengesAdmin = userChallenges?.filter(({ is_admin }) => is_admin);
  const programsAdmin = userPrograms?.filter(({ is_admin }) => is_admin);
  const [isAdminOfObjects, setIsAdminOfObjects] = useState(false);
  const [noResults, setNoResults] = useState(false);
  useEffect(() => {
    // set isAdmin to false if he is not admin of any type of object
    setIsAdminOfObjects(
      projectsAdmin?.length !== 0 ||
        groupsAdmin?.length !== 0 ||
        challengesAdmin?.length !== 0 ||
        userPrograms?.length !== 0
    );
    setNoResults(
      // if user has no object, set no result to true
      userProjects?.length === 0 &&
        userGroups?.length === 0 &&
        userChallenges?.length === 0 &&
        userPrograms?.length === 0
    );
  }, []);
  if (noResults) return <NoResults />;
  return (
    <Box position="relative">
      {isAdminOfObjects && ( // show the 2 different nav tabs only if user is admin of objects
        <Nav
          className="nav modal-nav-tabs container-fluid"
          style={{ flexWrap: 'nowrap', paddingRight: '1rem', minHeight: '70px' }}
        >
          <a className={`nav-item nav-link ${isAdminOfObjects && 'active'}`} href="#admin" data-toggle="tab">
            <FormattedMessage id="user.objects.adminOf" defaultMessage="I'm admin of" />
          </a>
          <a className={`nav-item nav-link ${!isAdminOfObjects && 'active'}`} href="#member" data-toggle="tab">
            <FormattedMessage id="user.objects.memberOf" defaultMessage="I'm member of" />
          </a>
        </Nav>
      )}
      <div className="tabContainer">
        <div className="tab-content">
          {/* Objects user is admin of */}
          <div className={`tab-pane ${isAdminOfObjects && 'active'}`} id="admin">
            <TabContent
              projects={projectsAdmin}
              groups={groupsAdmin}
              challenges={challengesAdmin}
              programs={programsAdmin}
            />
          </div>
          {/* Objects user is member of */}
          <div className={`tab-pane ${!isAdminOfObjects && 'active'}`} id="member">
            <TabContent
              projects={userProjects}
              groups={userGroups}
              challenges={userChallenges}
              programs={userPrograms}
            />
          </div>
        </div>
      </div>
    </Box>
  );
};

const TabContent = ({ projects, groups, challenges, programs }) => {
  return (
    <>
      {projects?.length !== 0 && ( // projects list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>
            <FormattedMessage id="user.profile.tab.projects" defaultMessage="Projects" />
          </h3>
          <ProjectList listProjects={projects} />
        </Box>
      )}
      {groups?.length !== 0 && ( // groups list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>
            <FormattedMessage id="user.profile.tab.communities" defaultMessage="Groups" />
          </h3>
          <CommunityList listCommunities={groups} />
        </Box>
      )}
      {challenges?.length !== 0 && ( // challenges list
        <Box mb={4} pb={8} borderBottom="2px solid lightgrey">
          <h3>
            <FormattedMessage id="general.challenges" defaultMessage="Challenges" />
          </h3>
          <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
            {challenges?.map((challenge, i) => (
              <ChallengeCard
                key={i}
                id={challenge.id}
                short_title={challenge.short_title}
                title={challenge.title}
                title_fr={challenge.title_fr}
                short_description={challenge.short_description}
                short_description_fr={challenge.short_description_fr}
                membersCount={challenge.members_count}
                needsCount={challenge.needs_count}
                has_saved={challenge.has_saved}
                clapsCount={challenge.claps_count}
                status={challenge.status}
                programId={challenge.program.id}
                programShortTitle={challenge.program.short_title}
                programTitle={challenge.program.title}
                programTitleFr={challenge.program.title_fr}
                projectsCount={challenge.projects_count}
                banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
              />
            ))}
          </Grid>
        </Box>
      )}
      {programs?.length !== 0 && ( // programs list
        <>
          <h3>
            <FormattedMessage id="general.programs" defaultMessage="Programs" />
          </h3>
          <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
            {programs?.map((program, i) => (
              <ProgramCard
                key={i}
                id={program.id}
                short_title={program.short_title}
                title={program.title}
                title_fr={program.title_fr}
                short_description={program.short_description}
                short_description_fr={program.short_description_fr}
                membersCount={program.members_count}
                needsCount={program.needs_count}
                has_saved={program.has_saved}
                clapsCount={program.claps_count}
                projectsCount={program.projects_count}
                banner_url={program.banner_url || '/images/default/default-program.jpg'}
              />
            ))}
          </Grid>
        </>
      )}
    </>
  );
};

export default UserMenu;
