import { css } from '@emotion/core';
import React from 'react';
import { space } from 'styled-system';
import styled from '~/utils/styled';

export const baseBtnStyle = (p) => css`
  background-color: ${
    p.btnType === 'secondary' ? 'white' : p.btnType === 'danger' ? p.theme.colors.danger : p.theme.colors.primary
  };
  border: 1px solid ${p.btnType === 'danger' ? p.theme.colors.danger : p.theme.colors.primary};
  color: ${p.btnType === 'secondary' ? p.theme.colors.primary : 'white'};
  display: inline-block;
  font-weight: 400;
  text-align: center;
  vertical-align: middle;
  cursor: ${p.disabled ? 'not-allowed' : 'pointer'};
  ${p.disabled && 'opacity: 0.5'};
  user-select: none;
  padding: 0.375rem 0.75rem;
  font-size: 1rem;
  line-height: 1.5;
  border-radius: 0.25rem;
  transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out;
  /* transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out,
    box-shadow 0.15s ease-in-out; */
  width: ${p.width ? p.width : 'fit-content'}};
  &:hover {
    ${p.btnType === 'secondary' && 'color: white'};
    ${p.btnType === 'secondary' && `background-color: ${p.theme.colors.primary}`};
    ${p.btnType !== 'secondary' && !p.disabled && 'opacity: 0.9'};
  }
`;

const Container = styled.button`
  ${space};
  ${baseBtnStyle}
`;
export default function Button(props) {
  return <Container {...props} p={[1]} />;
}
