// Add custom
import '@testing-library/jest-dom/extend-expect';
// add jest-emotion serializer
import { createSerializer, matchers } from 'jest-emotion';
import * as emotion from 'emotion';

expect.addSnapshotSerializer(createSerializer(emotion));

expect.extend(matchers);
