// TODO: Switch to the cookieconsent npm module.
import { useEffect } from 'react';

export default function useCookieConsent(locale) {
  useEffect(() => {
    // Cookie consent options TODO find better cookie consent plugin, and use directly from npm
    const content =
      locale === 'fr'
        ? {
            message: 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies',
            dismiss: 'Accepter',
            link: 'En savoir plus',
            href: 'https://jogl.io/terms-fr#cookie-policy',
          }
        : {
            message: 'This website uses cookies to ensure you get the best experience on our website.',
            dismiss: 'Got it',
            link: 'Learn more',
            href: 'https://jogl.io/fr/terms#cookie-policy',
          };

    if (window?.cookieconsent) {
      window.cookieconsent.initialise({
        palette: {
          popup: {
            background: '#38424F',
          },
          button: {
            background: '#2987cd',
          },
        },
        theme: 'classic',
        content,
      });
    }
  }, [locale]);
}
