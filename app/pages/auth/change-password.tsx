import Link from 'next/link';
import { useIntl } from 'react-intl';
import { NextPage } from 'next';
import FormChangePwd from '~/components/Tools/Forms/FormChangePwd';
import Layout from '~/components/Layout';
interface Props {}
const ChangePassword: NextPage<Props> = () => {
  const intl = useIntl();
  return (
    <Layout
      className="no-margin"
      title={`${intl.formatMessage({ id: 'auth.changePwd.title', defaultMessage: 'Change password' })} | JOGL`}
    >
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/" as="/">
            <a>
              <img src="/images/logo.svg" className="logo" alt="JoGL icon" />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <FormChangePwd />
        </div>
      </div>
    </Layout>
  );
};
export default ChangePassword;
