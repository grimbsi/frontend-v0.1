import { useState, useEffect, ReactNode } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Router, { useRouter } from 'next/router';
import Link from 'next/link';
import Alert from '~/components/Tools/Alert';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';

interface Props {}
const NewPwd: NextPage<Props> = () => {
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [error, setError] = useState('');
  const [fireRedirect, setFireRedirect] = useState(false);
  const [sending, setSending] = useState(false);
  const [success, setSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>(error);
  const intl = useIntl();
  const router = useRouter();
  const api = useApi();

  const checkPassword = (pwd, pwdConfirm) => {
    if (pwd !== pwdConfirm) {
      setError('err-4001');
      return false;
    }
    if (pwd.length < 8) {
      setError('err-4002');
      return false;
    }
    return true;
  };

  const handleChange = (e) => {
    setError('');
    switch (e.target.name) {
      case 'password':
        setPassword(e.target.value);
        break;
      case 'password_confirmation':
        setPasswordConfirmation(e.target.value);
        break;
      default:
        console.error(`The input with name=${e.target.name} is not handled`);
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const headers = {
      'access-token': router.query['access-token'],
      client: router.query.client,
      uid: router.query.uid,
    };
    if (checkPassword(password, passwordConfirmation)) {
      setError('');
      setSending(true);
      api
        .put('/api/auth/password', { password, password_confirmation: passwordConfirmation }, { headers })
        .then(() => {
          setSending(false);
          setSuccess(true);
          setTimeout(() => {
            setFireRedirect(true);
          }, 3500);
        })
        .catch((err) => {
          setError(err.response.data.errors[0]);
          setSending(false);
        });
    }
  };

  useEffect(() => {
    if (fireRedirect) {
      Router.push('/signin');
    }
  }, [fireRedirect]);

  useEffect(() => {
    setErrorMessage(
      error.includes('err-') ? <FormattedMessage id={error} defaultMessage="password not valid" /> : error
    );
  }, [error]);

  const title = {
    text: 'Password reset',
    id: 'auth.newPwd.title',
  };
  const msg = {
    text: 'Please enter a new password.',
    id: 'auth.newPwd.description',
  };
  return (
    <Layout
      className="no-margin"
      title={`${intl.formatMessage({ id: 'Reset Password', defaultMessage: 'Reset password' })} | JOGL`}
    >
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link href="/" as="/">
            <a>
              <img src="/images/logo.svg" className="logo" alt="JoGL icon" />
            </a>
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <div className="form-content">
            <div className="form-header">
              <h2 className="form-title" id="signModalLabel">
                <FormattedMessage id={title.id} defaultMessage={title.text} />
              </h2>
              <p>
                <FormattedMessage id={msg.id} defaultMessage={msg.text} />
              </p>
            </div>
            <div className="form-body" style={{ display: 'block' }}>
              <form onSubmit={handleSubmit} className="newPwdForm">
                <div className="form-group">
                  <label className="form-check-label" htmlFor="password">
                    <FormattedMessage id="auth.newPwd.pwd" defaultMessage="New password" />
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    placeholder={intl.formatMessage({
                      id: 'auth.newPwd.pwd.placeholder',
                      defaultMessage: 'Your Password',
                    })}
                    onChange={handleChange}
                  />
                </div>
                <div className="form-group">
                  <label className="form-check-label" htmlFor="password_confirmation">
                    <FormattedMessage id="auth.newPwd.pwdConfirm" defaultMessage="New password confirm" />
                  </label>
                  <input
                    type="password"
                    name="password_confirmation"
                    id="password_confirmation"
                    className="form-control"
                    placeholder={intl.formatMessage({
                      id: 'auth.newPwd.pwd.placeholder',
                      defaultMessage: 'Your Password',
                    })}
                    onChange={handleChange}
                  />
                </div>
                {error !== '' && <Alert type="danger" message={errorMessage} />}
                {success && (
                  <Alert
                    type="success"
                    message={<FormattedMessage id="info-4001" defaultMessage="New Password has been save !" />}
                  />
                )}

                <button className="btn btn-primary btn-block" disabled={!!sending} type="submit">
                  {sending && (
                    <>
                      <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true" />
                      <span className="sr-only">
                        {intl.formatMessage({ id: 'general.loading', defaultMessage: 'Loading...' })}
                      </span>
                      &nbsp;
                    </>
                  )}
                  <FormattedMessage id="auth.newPwd.btnConfirm" defaultMessage="Confirm my new password" />
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default NewPwd;
