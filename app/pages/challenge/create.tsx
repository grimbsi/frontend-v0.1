import { useState, useEffect, ReactNode } from 'react';
import { FormattedMessage } from 'react-intl';
import Router from 'next/router';
import Layout from '~/components/Layout';
import ChallengeForm from '~/components/Challenge/ChallengeForm';
import Alert from '~/components/Tools/Alert';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { NextPage } from 'next';

interface Props {}
const ChallengeCreate: NextPage<Props> = () => {
  const api = useApi();
  const [error, setError] = useState<string | ReactNode>('');
  const [loading, setLoading] = useState(true);
  const [sending, setSending] = useState(false);
  const { userData, userDataError } = useUserData();
  const [newChallenge, setNewChallenge] = useState({
    title: '',
    title_fr: '',
    short_title: '',
    short_description: '',
    short_description_fr: '',
    // interests: [],
    // skills: [],
    status: 'draft',
    creator_id: undefined,
  });
  // ? why is user passed as parameter?
  const { data: canCreate, error: cannotCreate } = useGet('/api/challenges/can_create', { user: userData });
  useEffect(() => {
    if (canCreate) {
      setLoading(false);
    }
    if (cannotCreate) {
      setLoading(false);
      Router.push('/challenge/forbidden', '/challenge/forbidden');
    }
  }, [canCreate, cannotCreate]);

  useEffect(() => {
    if (userData) {
      handleChange('creator_id', userData.id);
    }
  }, [userData]);

  const handleChange = (key, content) => {
    setNewChallenge((prev) => ({ ...prev, [key]: content }));
    setError('');
  };

  const handleSubmit = () => {
    if (
      newChallenge.title !== '' &&
      newChallenge.title_fr !== '' &&
      newChallenge.short_description !== '' &&
      newChallenge.short_description_fr !== '' &&
      newChallenge.short_title !== ''
      // newChallenge.skills.length > 0 &&
      // newChallenge.interests.length > 0
    ) {
      setSending(true);
      api
        .post('/api/challenges/', newChallenge)
        .then((res) => {
          setSending(false);
          Router.push('/challenge/[short_title]/edit', `/challenge/${res.data.short_title}/edit`);
        })
        .catch(() => {
          setSending(false);
        });
    } else {
      setError(<FormattedMessage id="challenge.create" defaultMessage="Some information is missing" />);
    }
  };

  return (
    <Layout title="Create a new challenge | JOGL">
      <Loading active={loading}>
        <div className="challengeCreate container-fluid">
          <h1>
            <FormattedMessage id="challenge.create.title" defaultMessage="Create a new challenge" />
          </h1>
          <ChallengeForm
            challenge={newChallenge}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
            mode="create"
            sending={sending}
          />
          {error !== '' && <Alert type="danger" message={error} />}
        </div>
      </Loading>
    </Layout>
  );
};
export default ChallengeCreate;
