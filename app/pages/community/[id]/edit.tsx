import { useState, useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import Router, { useRouter } from 'next/router';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Alert from '~/components/Tools/Alert';
import CommunityForm from '~/components/Community/CommunityForm';
import MembersList from '~/components/Members/MembersList';
import Layout from '~/components/Layout';
import { stickyTabNav, scrollToActiveTab } from '~/utils/utils';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import { NextPage } from 'next';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import Button from '~/components/primitives/Button';
import Translate from '~/utils/Translate';
import ReactGA from 'react-ga';
import { useModal } from '~/contexts/modalContext';
import P from '~/components/primitives/P';
import Box from '~/components/Box';

interface Props {
  community: {
    id: number;
    members_count: number;
    title: string;
    is_owner: boolean;
  };
}
const CommunityEdit: NextPage<Props> = ({ community: communityProp }) => {
  const [community, setCommunity] = useState(communityProp);
  const [sending, setSending] = useState(false);
  const [errors, setErrors] = useState('');
  const { showModal, setIsOpen } = useModal();
  const urlBack = { href: '/community/[id]/[[...index]]', as: `/community/${community?.id}/${community?.short_title}` };
  const router = useRouter();
  const api = useApi();

  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit');
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const handleChange = (key, content) => {
    setCommunity((prevCommunity) => ({ ...prevCommunity, [key]: content }));
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/communities/${community.id}`, { community }).catch(() => setSending(false));
    if (res) {
      setSending(false);
      Router.push({ pathname: urlBack.href, query: { success: 1 } }, urlBack.as).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
    }
  };

  const deleteGroup = async () => {
    const res = await api.delete(`api/communities/${community.id}/`).catch((error) => {
      setErrors(error.toString());
    });
    if (res) {
      ReactGA.event({ category: 'Group', action: 'delete', label: `${community.id}` }); // record event to Google Analytics
      setIsOpen(false); // close modal
      Router.push('/search/[active-index]', '/search/groups');
    }
  };
  const errorMessage = errors.includes('err-') ? (
    <FormattedMessage id={errors} defaultMessage="An error has occurred" />
  ) : (
    errors
  );
  const delBtnTitleId = community.members_count > 1 ? 'community.archive.title' : 'community.delete.title';
  const delBtnTitle = community.members_count > 1 ? 'Archive group' : 'Delete group';
  const delBtnTextId = community.members_count > 1 ? 'community.archive.text' : 'community.delete.text';
  const delBtnText =
    community.members_count > 1
      ? 'Are you sure you want to archive this group?'
      : 'Are you sure you want to delete this group?  It will be permanently deleted.';

  return (
    <Layout title={`${community.title} | JOGL`}>
      <div className="communityEdit container-fluid">
        <h1>
          <FormattedMessage id="community.edit.title" defaultMessage="Edit my Community" />
        </h1>
        <Link href={urlBack.href} as={urlBack.as}>
          <a>
            {/* go back link */}
            <FontAwesomeIcon icon="arrow-left" />
            <FormattedMessage id="community.edit.back" defaultMessage="Go back" />
          </a>
        </Link>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <FormattedMessage id="entity.tab.basic_info" defaultMessage="Information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="entity.tab.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            <div className="tab-pane active" id="basic_info">
              <CommunityForm
                community={community}
                mode="edit"
                sending={sending}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
              />
            </div>
            <div className="tab-pane" id="members">
              <MembersList
                itemType="communities"
                itemId={parseInt(router.query.id as string)}
                isOwner={community.is_owner}
              />
            </div>
            <div className="tab-pane" id="advanced">
              <ManageExternalLink itemType="communities" itemId={router.query.id} />
              <hr />
              <div className="deleteBtns">
                {community && community.members_count > 1 && (
                  <p>
                    <FormattedMessage id="community.delete.explain" />
                  </p>
                )}
                <Button
                  onClick={() => {
                    showModal({
                      children: (
                        <>
                          {errors && <Alert type="danger" message={errorMessage} />}
                          <P fonSize="1rem">
                            <Translate id={delBtnTextId} defaultMessage={delBtnText} />
                          </P>
                          <Box row spaceX={3}>
                            <Button btnType="danger" onClick={deleteGroup}>
                              <FormattedMessage id="general.yes" defaultMessage="Yes" />
                            </Button>
                            <Button onClick={() => setIsOpen(false)}>
                              <FormattedMessage id="general.no" defaultMessage="No" />
                            </Button>
                          </Box>
                        </>
                      ),
                      title: delBtnTitle,
                      titleId: delBtnTitleId,
                      maxWidth: '30rem',
                    });
                  }}
                  btnType="danger"
                >
                  <Translate id={delBtnTitleId} defaultMessage={delBtnTitle} />
                </Button>
                {community && community.members_count > 1 && (
                  <Button btnType="danger" disabled ml={3}>
                    <Translate id="community.delete.title" defaultMessage="Delete group" />
                  </Button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

CommunityEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const res = await api
    .get(`/api/communities/${query.id}`)
    .catch((err) => console.error(`Couldn't fetch community with id=${query.id}`, err));
  // Check if it got the community and if the user is admin
  if (res?.data?.is_admin) return { community: res.data };
  isomorphicRedirect(ctx, '/search/[active-index]', '/search/groups');
};

export default CommunityEdit;
