import { useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { NextPage } from 'next';
import Box from '~/components/Box';
import Card from '~/components/Card';
import NeedUpdate from '~/components/Need/NeedUpdate';
import NeedContent from '~/components/Need/NeedContent';
export type NeedDisplayMode = 'details' | 'update' | 'create';

interface Props {
  need: {
    title: string;
    end_date: string;
    content: string;
    project: {
      banner_url: string;
    };
  };
}
const Needs: NextPage<Props> = ({ need: needProp }) => {
  const [need, setNeed] = useState(needProp);
  const [mode, setMode] = useState();
  const api = useApi();
  const { query } = useRouter();
  const refresh = async () => {
    const newNeed = await getNeedApi(api, query.id).catch((err) => console.error(err));
    setNeed(newNeed);
  };
  const changeMode = (newMode) => {
    if (newMode) {
      setMode(newMode);
      refresh();
    }
  };
  return (
    <Layout title={`Need: ${need?.title} | JOGL`} desc={`${need?.content}`} img={need?.project.banner_url}>
      <Box px={2} maxWidth="800px" width="100%" margin="auto">
        <Card>
          {mode === 'update' ? (
            <NeedUpdate changeMode={changeMode} need={need} refresh={refresh} />
          ) : (
            <NeedContent changeMode={changeMode} need={need} refresh={refresh} />
          )}
        </Card>
      </Box>
    </Layout>
  );
};

const getNeedApi = async (api, id) => {
  const res = await api.get(`/api/needs/${id}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
};
Needs.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const need = await getNeedApi(api, ctx.query.id);
  if (need) return { need };
  isomorphicRedirect(ctx, '/search/[active-index]', '/search/needs');
};

export default Needs;
