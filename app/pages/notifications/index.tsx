import { useContext, useState, useEffect, useCallback } from 'react';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import { UserContext } from '~/contexts/UserProvider';
import styled from '~/utils/styled';
import A from '~/components/primitives/A';
import { FormattedMessage } from 'react-intl';
import Link from 'next/link';
import Button from '~/components/primitives/Button';
import Loading from '~/components/Tools/Loading';
import { displayObjectRelativeDate } from '~/utils/utils';

const NotificationPage = () => {
  const [notifications, setNotifications] = useState([]);
  const [unreadNotificationsCount, setUnreadNotificationsCount] = useState(0);
  const [totalNotificationsCount, setTotalNotificationsCount] = useState(0);
  const [loadingBtn, setLoadingBtn] = useState(false);
  const [offSetCursor, setOffSetCursor] = useState();
  const numberOfNotifPerCall = 30;
  const api = useApi();
  const { user } = useContext(UserContext);

  const loadNotifications = useCallback(() => {
    setLoadingBtn(true);
    const pageScroll = window.pageYOffset; // get whole page offSetTop
    const paginationFilters = !offSetCursor // if we don't have offSetCursor, it means it's first call, so call only first number of wanted notifications
      ? `first: ${numberOfNotifPerCall}`
      : // else, it has been set, so call wanted number of notifications, after the last one (so we have a pagination)
        `first: ${numberOfNotifPerCall} after: "${offSetCursor}"`;
    user &&
      api
        .post('/api/graphql/', {
          query: `
          query {
            user (id: ${user.id} ) {
              id,
              email,
              notifications(${paginationFilters}) {
                unreadCount
                totalCount
                edges {
                  node {
                    id
                    link
                    read
                    subjectLine
                    createdAt
                  }
                  cursor
                }
              }
            }
          }
        `,
        })
        .then((response) => {
          var userNotifications = response.data.data.user.notifications;
          // set offset cursor to the last notification cursor value, so we can call the next notifications after that one
          setOffSetCursor(userNotifications?.edges[userNotifications.edges.length - 1].cursor);
          // update notification counts
          setUnreadNotificationsCount(userNotifications.unreadCount);
          setTotalNotificationsCount(userNotifications.totalCount);
          // concatenate current notifications with the new ones
          setNotifications([...notifications, ...userNotifications.edges.map((edge) => edge.node)]);
          window.scrollTo(0, pageScroll); // force to stay at pageScroll (else it would focus you at bottom of the list of posts)
          setLoadingBtn(false);
        })
        .catch((err) => {});
  }, [user, notifications]);

  useEffect(() => {
    // get first notifications on first load
    loadNotifications();
  }, [user]);

  const visitNotificationLinkAndMarkAsRead = (notification) => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markNotificationAsRead(input: {id: ${notification.id}}) {
              notification {
                id
                read
              }
            }
          }`,
      })
      .then((_response) => {
        // On success, update unread count
        notification.read = true;
        setUnreadNotificationsCount(unreadNotificationsCount - 1);
      })
      .catch((err) => {
        // do something
      });
  };

  const markAllNotificationsAsRead = () => {
    api
      .post('/api/graphql/', {
        query: `
          mutation {
            markAllNotificationsAsRead(input: {}) {
              unreadCount
            }
          }`,
      })
      .then((response) => {
        // On success, update unread count
        setUnreadNotificationsCount(response.data.unreadCount);
        let notifs = notifications.map((notif) => {
          notif.read = true;
          return notif;
        });
        setNotifications(notifs);
      })
      .catch((err) => {
        // do something
      });
  };

  return (
    <Layout title="Notifications | JOGL">
      <Box px={2} maxWidth="800px" width="100%" margin="auto">
        <Box row justifyContent="space-between" alignItems="center" py={2}>
          <Box fontWeight="bold">
            <FormattedMessage id="settings.notifications.title" defaultMessage="Notifications" />
          </Box>
          <Box row borderTop="none" alignItems="flex-end">
            <Box borderTop="none" onClick={() => markAllNotificationsAsRead()}>
              <A href="#">
                <FormattedMessage id="settings.notifications.markAsRead" defaultMessage="Mark All as read" />
              </A>
            </Box>
            <Box px={1}>.</Box>
            <Box>
              <Link href="/user/[id]/settings" as={`/user/${user?.id}/settings`} passHref>
                <A>
                  <FormattedMessage id="menu.profile.settings" defaultMessage="Settings" />
                </A>
              </Link>
            </Box>
          </Box>
        </Box>
        {notifications?.length === 0 ? (
          <Loading />
        ) : (
          <Box border="1px solid grey">
            {notifications &&
              notifications?.map((notification) => {
                return (
                  <Notification
                    as="a"
                    href={notification.link}
                    key={notification.id}
                    hasRead={notification.read}
                    onClick={() => visitNotificationLinkAndMarkAsRead(notification)}
                  >
                    <Box>
                      <Box pr={3}>{notification.subjectLine}</Box>
                      <Box color="#797979">{displayObjectRelativeDate(notification.createdAt)}</Box>
                    </Box>
                  </Notification>
                );
              })}
          </Box>
        )}
        {notifications.length !== totalNotificationsCount && (
          <Box alignSelf="center" mt={4}>
            <Button onClick={() => loadNotifications()}>
              {loadingBtn && (
                <>
                  <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                  &nbsp;
                </>
              )}
              <FormattedMessage id="general.load" defaultMessage="Load more" />
            </Button>
          </Box>
        )}
      </Box>
    </Layout>
  );
};

const Notification = styled(Box)`
  background: ${(p) => (p.hasRead ? 'white' : '#EDF2FA')};
  border-bottom: ${(p) => `1px solid ${p.theme.colors.greys['500']}!important`};
  padding: 5px 12px;
  color: black;
  &:hover {
    background: ${(p) => (p.hasRead ? '#f6f6f6' : '#e2e7ee')};
    color: inherit;
    text-decoration: none;
  }
`;

export default NotificationPage;
