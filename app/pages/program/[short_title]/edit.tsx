import React, { useState, useEffect, useMemo } from 'react';
import { FormattedMessage } from 'react-intl';
import Router, { useRouter } from 'next/router';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Layout from '~/components/Layout';
import ProgramForm from '~/components/Program/ProgramForm';
import MembersList from '~/components/Members/MembersList';
import ChallengeAdminCard from '~/components/Challenge/ChallengeAdminCard';
import { stickyTabNav, scrollToActiveTab } from '~/utils/utils';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import Grid from '~/components/Grid';
import { NextPage } from 'next';
import Button from '~/components/primitives/Button';
import { useModal } from '~/contexts/modalContext';
import Translate from '~/utils/Translate';
import Alert from '~/components/Tools/Alert';
import ManageFaq from '~/components/Tools/ManageFaq';
import ManageBoards from '~/components/Program/ManageBoards';
import ManageResources from '~/components/Program/ManageResources';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import Box from '~/components/Box';
import Loading from '~/components/Tools/Loading';

interface Props {
  program: {
    id: number;
    title: string;
    short_title: string;
    is_owner: boolean;
  };
}
const ProgramEdit: NextPage<Props> = ({ program: programProp }) => {
  const [program, setProgram] = useState(programProp);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { showModal, setIsOpen } = useModal();
  const router = useRouter();
  const { data: challengesData, revalidate: challengesRevalidate, mutate: mutateChallenges } = useGet(
    `/api/programs/${program.id}/challenges`
  );
  useEffect(() => {
    setTimeout(() => {
      stickyTabNav('isEdit'); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
  }, [router]);

  const handleChange = (key, content) => {
    setProgram((prevProgram) => ({ ...prevProgram, [key]: content }));
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/programs/${program.id}`, { program }).catch((err) => {
      console.error(`Couldn't patch program with id=${program.id}`, err);
      setSending(false);
    });
    if (res) {
      // on success
      setSending(false);
      Router.push(
        { pathname: '/program/[short_title]', query: { success: 1 } },
        `/program/${program.short_title}`
      ).then(() => window.scrollTo(0, 0)); // force scroll to top of page @TODO might not need anymore once this issue will be fixed: https://github.com/vercel/next.js/issues/15206
    }
  };
  return (
    <Layout title={`${program.title} | JOGL`}>
      <div className="programEdit">
        <Box px={4}>
          <h1>
            <FormattedMessage id="program.edit.title" defaultMessage="Edit the program" />
          </h1>
          <Link href="/program/[short_title]" as={`/program/${program.short_title}`}>
            <a>
              <FontAwesomeIcon icon="arrow-left" />
              <FormattedMessage id="program.edit.back" defaultMessage="Go back" />
            </a>
          </Link>
        </Box>
        <nav className="nav nav-tabs container-fluid">
          <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
            <FormattedMessage id="entity.tab.basic_info" defaultMessage="Basic information" />
          </a>
          <a className="nav-item nav-link" href="#members" data-toggle="tab">
            <FormattedMessage id="entity.tab.members" defaultMessage="Members" />
          </a>
          <a className="nav-item nav-link" href="#challenges" data-toggle="tab">
            <FormattedMessage id="general.challenges" defaultMessage="Challenges" />
          </a>
          <a className="nav-item nav-link" href="#faqs" data-toggle="tab">
            <FormattedMessage id="faq.title" defaultMessage="FAQs" />
          </a>
          <a className="nav-item nav-link" href="#boards" data-toggle="tab">
            <FormattedMessage id="program.boardTitle" defaultMessage="Boards" />
          </a>
          <a className="nav-item nav-link" href="#Resources" data-toggle="tab">
            <FormattedMessage id="program.resourcesTitle" defaultMessage="Resources" />
          </a>
          <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
            <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
          </a>
        </nav>
        <div className="tabContainer">
          <div className="tab-content justify-content-center container-fluid">
            <div className="tab-pane active" id="basic_info">
              <ProgramForm
                mode="edit"
                program={program}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                sending={sending}
              />
            </div>
            <div className="tab-pane" id="members">
              {program.id && <MembersList itemType="programs" itemId={program.id} isOwner={program.is_owner} />}
            </div>
            <div className="tab-pane" id="challenges">
              {challengesData ? (
                <div className="challengesAttachedList">
                  <div className="justify-content-end challengesAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <LinkChallengeModal
                              alreadyPresentChallenges={challengesData?.challenges}
                              programId={program.id}
                              mutateChallenges={mutateChallenges}
                              closeModal={() => setIsOpen(false)}
                            />
                          ),
                          title: 'Submit a challenge',
                          titleId: 'attach.challenge.button.title',
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      <Translate id="attach.challenge.button.title" defaultMessage="Submit a challenge" />
                    </Button>
                  </div>
                  {challengesData?.challenges.length !== 0 ? (
                    <Grid gridCols={1} display={['grid', 'inline-grid']} pt={8}>
                      {challengesData?.challenges.map((challenge, i) => (
                        <ChallengeAdminCard challenge={challenge} key={i} callBack={() => challengesRevalidate()} />
                      ))}
                    </Grid>
                  ) : (
                    <Loading />
                  )}
                </div>
              ) : (
                <Loading />
              )}
            </div>
            <div className="tab-pane" id="faqs">
              <ManageFaq itemType="programs" itemId={program.id} />
            </div>
            <div className="tab-pane" id="boards">
              <ManageBoards itemType="programs" itemId={program.id} />
            </div>
            <div className="tab-pane" id="Resources">
              <ManageResources itemType="programs" itemId={program.id} />
            </div>
            <div className="tab-pane" id="advanced">
              <ManageExternalLink itemType="programs" itemId={program.id} />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

ProgramEdit.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/programs/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch program with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const programRes = await api
      .get(`/api/programs/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch program with id=${getIdRes.data.id}`, err));
    // Check if it got the program and if the user is admin
    if (programRes?.data?.is_admin) return { program: programRes.data };
  }
  isomorphicRedirect(ctx, '/', '/');
};

interface PropsModal {
  alreadyPresentChallenges: any[];
  programId: number;
  mutateChallenges: (data?: any, shouldRevalidate?: boolean) => Promise<any>;
  closeModal: () => void;
}
export const LinkChallengeModal: FC<PropsModal> = ({
  alreadyPresentChallenges,
  programId,
  mutateChallenges,
  closeModal,
}) => {
  const { data: dataChallengesMine, error } = useGet('/api/challenges/mine');
  const [selectedChallenge, setSelectedChallenge] = useState();
  const [sending, setSending] = useState(false);
  const [requestSent, setRequestSent] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const api = useApi();

  // Filter the challenges that are already in this challenge so you don't add it twice!
  const filteredChallenges = useMemo(() => {
    if (dataChallengesMine) {
      return dataChallengesMine.filter((challengeMine) => {
        // Check if my challenge is found in alreadyPresentChallenges
        const isMyChallengeAlreadyPresent = alreadyPresentChallenges.find((alreadyPresentChallenge) => {
          return alreadyPresentChallenge.id === challengeMine.id;
        });
        // We keep only the ones that are not present
        return !isMyChallengeAlreadyPresent;
      });
    }
  }, [dataChallengesMine, alreadyPresentChallenges]);

  const onSubmit = async (e) => {
    e.preventDefault();
    setSending(true);
    // Link this challenge to the challenge then mutate the cache of the challenges from the parent prop
    if ((selectedChallenge as { id: number })?.id) {
      await api
        .put(`/api/programs/${programId}/challenges/${(selectedChallenge as { id: number }).id}`)
        .catch(() =>
          console.error(`Could not PUT/link programId=${programId} with challenge challengeId=${selectedChallenge.id}`)
        );
      setSending(false);
      setRequestSent(true);
      setIsButtonDisabled(true);
      mutateChallenges({ challenges: [...alreadyPresentChallenges, selectedChallenge] });
      setTimeout(() => {
        // close modal after 3.5sec
        closeModal();
      }, 3500);
    }
  };
  const onChallengeselect = (e) => {
    setSelectedChallenge(filteredChallenges.find((item) => item.id === parseInt(e.target.value)));
    setIsButtonDisabled(false);
  };

  return (
    <div>
      {filteredChallenges && filteredChallenges.length > 0 ? (
        <form style={{ textAlign: 'left' }}>
          {filteredChallenges.map((challenge, index) => (
            <div className="form-check" key={index} style={{ height: '50px' }}>
              <input
                type="radio"
                className="form-check-input"
                name="exampleRadios"
                id={`challenge-${index}`}
                value={challenge.id}
                onChange={onChallengeselect}
              />
              <label className="form-check-label" htmlFor={`challenge-${index}`}>
                {challenge.title}
              </label>
            </div>
          ))}

          <div className="btnZone">
            <Button type="submit" disabled={isButtonDisabled || sending} onClick={onSubmit} mb={2}>
              <>
                {sending && (
                  <>
                    <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true" />
                    &nbsp;
                  </>
                )}
                <FormattedMessage id="attach.challenge.btnSend" defaultMessage="Submit this challenge" />
              </>
            </Button>
            {requestSent && (
              <Alert
                type="success"
                message={
                  <FormattedMessage id="attach.challenge.btnSendEnded" defaultMessage="challenge was submitted" />
                }
              />
            )}
          </div>
        </form>
      ) : (
        <div className="noChallenge" style={{ textAlign: 'center' }}>
          <FormattedMessage id="attach.challenge.noChallenge" defaultMessage="You do not have a challenge to add" />
        </div>
      )}
    </div>
  );
};

export default ProgramEdit;
