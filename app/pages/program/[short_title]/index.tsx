/* eslint-disable camelcase */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { flexbox, layout, space, typography } from 'styled-system';
import Box from '~/components/Box';
import Feed from '~/components/Feed/Feed';
import PostDisplayMinimal from '~/components/Feed/Posts/PostDisplayMinimal';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import A from '~/components/primitives/A';
import Button from '~/components/primitives/Button';
import H1 from '~/components/primitives/H1';
import H2 from '~/components/primitives/H2';
import P from '~/components/primitives/P';
import ProgramAbout from '~/components/Program/ProgramAbout';
import ProgramFaq from '~/components/Program/ProgramFaq';
import ProgramHeader from '~/components/Program/ProgramHeader';
import ProgramHome from '~/components/Program/ProgramHome';
import ProgramMembers from '~/components/Program/ProgramMembers';
import ProgramNeeds from '~/components/Program/ProgramNeeds';
import ProgramProjects from '~/components/Program/ProgramProjects';
import ProgramResources from '~/components/Program/ProgramResources';
import BtnFollow from '~/components/Tools/BtnFollow';
import BtnSave from '~/components/Tools/BtnSave';
import { ContactForm } from '~/components/Tools/ContactForm';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import Loading from '~/components/Tools/Loading';
import NoResults from '~/components/Tools/NoResults';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import useMembers from '~/hooks/useMembers';
import useNeeds from '~/hooks/useNeeds';
import useUserData from '~/hooks/useUserData';
import { Challenge, Faq } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import { useScrollHandler } from '~/utils/utils';
import InviteMember from '~/components/Tools/InviteMember';
import ChallengeCard from '~/components/Challenge/ChallengeCard';
import ReactGA from 'react-ga';
import ReactTooltip from 'react-tooltip';

const DEFAULT_TAB = 'home';
interface Props {
  program: {
    id: number;
    title: string;
    short_description: string;
    short_description_fr?: string;
    banner_url: string;
    feed_id: number;
    is_admin: boolean;
    is_member: boolean;
    is_owner: boolean;
    description: string;
    description_fr?: string;
    enablers: string;
    contact_email: string;
    meeting_information: string;
    logo_url: string;
    title_fr: string;
    has_saved: boolean;
    has_followed: boolean;
    launch_date: Date;
    status: string;
  };
}
const Program: NextPage<Props> = ({ program }) => {
  const router = useRouter();
  const { locale, formatMessage } = useIntl();
  const { showModal, setIsOpen } = useModal();
  const { members } = useMembers('programs', program?.id, undefined);
  const [selectedTab, setSelectedTab] = useState(DEFAULT_TAB);
  const [offSetTop, setOffSetTop] = useState();
  const [tabs, setTabs] = useState([
    { value: 'home', intlId: 'program.home', defaultMessage: 'Home' },
    { value: 'about', intlId: 'program.tab.about', defaultMessage: 'About' },
    { value: 'news', intlId: 'entity.tab.news', defaultMessage: 'News' },
    { value: 'challenges', intlId: 'entity.tab.challenges', defaultMessage: 'Challenges' },
    { value: 'projects', intlId: 'entity.tab.projects', defaultMessage: 'Projects' },
    { value: 'needs', intlId: 'entity.tab.needs', defaultMessage: 'Needs' },
    { value: 'members', intlId: 'entity.tab.participants', defaultMessage: 'Participants' },
    { value: 'resources', intlId: 'program.tab.resources', defaultMessage: 'Resources' },
  ]);
  const { dataNeeds } = useNeeds('programs', program?.id);
  const { data: dataPosts, revalidate: revalidatePost } = useGet(`/api/feeds/${program?.feed_id}?items=5&page=1`);
  const { data: dataChallenges } = useGet<{ challenges: Challenge }>(`/api/programs/${program?.id}/challenges`);
  const { data: dataExternalLink } = useGet(`/api/programs/${program?.id}/links`);
  const { data: faqList } = useGet<{ documents: Faq[] }>(`/api/programs/${program.id}/faq`);
  const { userData } = useUserData();
  const theme = useTheme();
  const headerRef = useRef();
  const navRef = useRef();
  const isSticky = useScrollHandler(offSetTop);
  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  useEffect(() => {
    // on first load, check if url has a tab param
    if (router.query.tab) {
      // if it does, select this tab
      setSelectedTab(router.query.tab as string);
    }
  }, []);

  // add faq tab only if has faq
  useEffect(() => {
    tabs[8]?.value !== 'faq' &&
      faqList &&
      faqList.documents.length !== 0 &&
      setTabs([...tabs, { value: 'faq', intlId: 'entity.info.faq', defaultMessage: 'FAQs' }]); // don't add another time if tab already exists
  }, [faqList]);

  const contactEmail = program?.contact_email || 'hello@jogl.io';

  // function when changing tab (or when router change in general)
  useEffect(() => {
    const tabValues = tabs.map(({ value }) => value);
    if (tabValues.includes(router.query.tab as string)) {
      if (router.query.tab === 'home') {
        router.push(`/program/${router.query.short_title}`, undefined, { shallow: true });
      }
      const navContainerTopPosition = navRef.current.getBoundingClientRect().top + window.scrollY; // get y position of nav container
      const yAdjustment = window.innerWidth < 768 ? 40 : 150; // change yAdustement depending on if it's mobile/tablet or desktop
      window.scrollTo(0, navContainerTopPosition - yAdjustment); // force scroll to top of the nav/tab (remove a little to be really on top)
      setSelectedTab(router.query.tab as string);
    }
    if (!router.query.tab) setSelectedTab(DEFAULT_TAB);
  }, [router]);

  const StickyHeading = styled(Box)`
    position: fixed;
    height: 70px;
    top: ${() => (isSticky ? '64px' : '-1000px')};
    width: 100%;
    z-index: 9;
    border-top: 1px solid grey;
    left: 0;
    transition: top 333ms;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
    @media (max-width: ${(p) => p.theme.breakpoints.md}) {
      height: 60px;
      .actions {
        display: none;
      }
      img {
        width: 40px;
        height: 40px;
      }
    }
    > div {
      max-width: 1280px;
      margin: 0 auto;
      width: 100%;
    }
  `;

  const sendMessageToAdmin = (id, first_name, last_name) => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={() => setIsOpen(false)} />,
      title: 'Send message to {userFullName}',
      titleId: 'user.contactModal.title',
      values: { userFullName: first_name + ' ' + last_name },
    });
  };

  return (
    <Layout
      title={program?.title && `${program.title} | JOGL`}
      desc={program?.short_description}
      img={program?.banner_url || '/images/default/default-program.jpg'}
      className="small-top-margin"
    >
      <Img src={program?.banner_url || '/images/default/default-program.jpg'} width="100%" />
      <Grid
        display="grid"
        gridTemplateColumns={['100%', undefined, '20rem calc(100% - 20rem)', undefined, '25% 75%']}
        bg="white"
        pb={4}
        px={[4, undefined, undefined, undefined, 0]}
      >
        <Box alignItems={[undefined, undefined, 'center']} mt={[0, undefined, '-90px', '-100px']} ref={headerRef}>
          <ProgramHeader program={program} lang={locale} />
        </Box>
        <Box pl={[0, undefined, 6]} py={3} alignSelf="center">
          <Text>
            {locale === 'fr' && program?.short_description_fr
              ? program?.short_description_fr
              : program?.short_description}
          </Text>
        </Box>
      </Grid>
      <StickyHeading className="stickyHeading" bg="white" justifyContent="center">
        <Box row alignItems="center" px={[4, undefined, undefined, undefined, 0]}>
          <LogoImg src={program?.logo_url || 'images/jogl-logo.png'} mr={4} alt="program logo" />
          <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px">
            {locale === 'fr' && program?.title_fr ? program?.title_fr : program?.title}
          </H1>
          <Box row pl={5} className="actions">
            {userData && (
              <>
                <Box pr={5}>
                  <BtnSave itemType="programs" itemId={program.id} saveState={program.has_saved} />
                </Box>
                <BtnFollow followState={program?.has_followed} itemType="programs" itemId={program.id} />
              </>
            )}
            <ShareBtns type="program" />
          </Box>
        </Box>
      </StickyHeading>
      <Grid
        display="grid"
        gridTemplateColumns={['100%', undefined, '20rem calc(100% - 20rem)', undefined, '25% 50% 25%']}
        bg={[theme.colors.lightBlue, undefined, 'white']}
        ref={navRef}
      >
        {/* Header and nav (left col on desktop, top col on mobile */}
        <Box
          alignItems={[undefined, undefined, 'center']}
          bg="white"
          position="sticky"
          top={['40px', undefined, '50px']}
          zIndex={8}
        >
          <NavContainer bg="white">
            {/* quick actions buttons */}
            <Box row justifyContent="center" spaceX={6} mb={4}>
              <QuickAction
                as="button"
                onClick={() =>
                  router.push('/program/[short_title]?tab=projects', `/program/${program.short_title}?tab=projects`)
                }
              >
                <QuickActionIcon>
                  <P mb={0} color={theme.colors.greys['600']} fontSize={8}>
                    +
                  </P>
                </QuickActionIcon>
                {formatMessage({ id: 'program.getStarted', defaultMessage: 'Get started' })}
              </QuickAction>
              <QuickAction
                as="button"
                onClick={() =>
                  router.push('/program/[short_title]?tab=about', `/program/${program.short_title}?tab=about`)
                }
              >
                <QuickActionIcon fontSize={2}>
                  <FontAwesomeIcon icon="eye" color={theme.colors.greys['600']} />
                </QuickActionIcon>
                {formatMessage({ id: 'program.learnMore', defaultMessage: 'Learn more' })}
              </QuickAction>
            </Box>
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="left" />
            <OverflowGradient display={[undefined, undefined, 'none']} gradientPosition="right" />
            <Nav
              as="nav"
              flexDirection={['row', undefined, 'column']}
              spaceX={[3, undefined, 0]}
              width="100%"
              bg="white"
            >
              {tabs.map((item, index) => (
                <Link
                  shallow
                  scroll={false}
                  key={index}
                  href={`/program/[short_title]${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                  as={`/program/${router.query.short_title}${item.value === DEFAULT_TAB ? '' : `?tab=${item.value}`}`}
                >
                  <Tab selected={item.value === selectedTab} px={[1, undefined, 3]} py={3} as="button">
                    <FormattedMessage id={item.intlId} defaultMessage={item.defaultMessage} />
                  </Tab>
                </Link>
              ))}
            </Nav>
          </NavContainer>
        </Box>

        {/* Main content, that change content depending on the selected tab (middle content on desktop) */}
        <Box px={[0, undefined, 4]}>
          {selectedTab === 'home' && (
            <TabPanel id="home" px={[3, 4, undefined, 0]} pt={[5, undefined, 0]}>
              <DesktopBorders>
                <ProgramHome
                  shortDescription={
                    locale === 'fr' && program?.short_description_fr
                      ? program?.short_description_fr
                      : program?.short_description
                  }
                  programId={program.id}
                  programFeedId={program.feed_id}
                  meetingInfo={program.meeting_information}
                  isAdmin={program.is_admin}
                  posts={dataPosts}
                  needs={dataNeeds?.needs}
                  challenges={dataChallenges?.challenges}
                  refreshPost={revalidatePost}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'news' && (
            <TabPanel id="news" style={{ margin: '0 auto' }} width="100%" pt={[2, undefined, 0]}>
              {
                //  Show feed, and pass DisplayCreate to admins
                program.feed_id && (
                  <Feed
                    feedId={program.feed_id}
                    displayCreate={program.is_admin || program.is_owner}
                    isAdmin={program.is_admin || program.is_owner}
                  />
                )
              }
            </TabPanel>
          )}
          {selectedTab === 'challenges' && (
            <TabPanel id="challenges" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <P>
                {formatMessage({
                  id: 'program.challenges.text',
                  defaultMessage:
                    'To participate in this program, participate in at least one of its challenges. Check out the projects already submitted, contribute in them or create your own!',
                })}
                <br />
                {formatMessage({
                  id: 'program.home.challengeCta',
                  defaultMessage:
                    'Get involved in Challenges by submitting a project, evaluating submissions, & voting',
                })}
                .
              </P>
              <Box py={4} position="relative">
                {!dataChallenges ? (
                  <Loading />
                ) : dataChallenges?.challenges.length === 0 ? (
                  <NoResults type="challenge" />
                ) : (
                  <Grid gridGap={4} gridCols={[1, 2, 1, 2]} display={['grid', 'inline-grid']} py={4}>
                    {dataChallenges.challenges
                      ?.filter(({ status }) => status !== 'draft')
                      // don't display draft challenges
                      .map((challenge, index) => (
                        <ChallengeCard
                          key={index}
                          id={challenge.id}
                          banner_url={challenge.banner_url || '/images/default/default-challenge.jpg'}
                          short_title={challenge.short_title}
                          title={challenge.title}
                          title_fr={challenge.title_fr}
                          short_description={challenge.short_description}
                          short_description_fr={challenge.short_description_fr}
                          membersCount={challenge.members_count}
                          needsCount={challenge.needs_count}
                          clapsCount={challenge.claps_count}
                          projectsCount={challenge.projects_count}
                          status={challenge.status}
                          has_saved={challenge.has_saved}
                        />
                      ))}
                  </Grid>
                )}
              </Box>
            </TabPanel>
          )}
          {selectedTab === 'projects' && (
            <TabPanel id="projects" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ProgramProjects programId={program.id} />
            </TabPanel>
          )}
          {selectedTab === 'needs' && (
            <TabPanel id="needs" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <ProgramNeeds programId={program.id} />
            </TabPanel>
          )}
          {selectedTab === 'members' && (
            <TabPanel id="members" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]} position="relative">
              <ProgramMembers programId={program.id} />
            </TabPanel>
          )}

          {selectedTab === 'about' && (
            <TabPanel id="about" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramAbout
                  programId={program?.id}
                  enablers={program.enablers}
                  contactEmail={contactEmail}
                  description={
                    locale === 'fr' && program?.description_fr ? program?.description_fr : program?.description
                  }
                  dataChallenges={dataChallenges}
                  launchDate={program?.launch_date}
                  status={program?.status}
                />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'faq' && (
            <TabPanel id="faq" px={[3, 4, undefined, 0]} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramFaq faqList={faqList} />
              </DesktopBorders>
            </TabPanel>
          )}
          {selectedTab === 'resources' && (
            <TabPanel id="resources" px={0} pt={[7, undefined, undefined, 0]}>
              <DesktopBorders>
                <ProgramResources programId={program?.id} />
              </DesktopBorders>
            </TabPanel>
          )}
          <Box pt={11} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <H2>{formatMessage({ id: 'member.invite.general', defaultMessage: 'Invite someone to join!' })}</H2>
            <InviteMember itemType="programs" itemId={program.id} />
          </Box>
          <Box pt={8} pb={7} px={4} display={['block', undefined, undefined, undefined, 'none']}>
            <CtaAndLinks
              router={router}
              contactEmail={contactEmail}
              dataExternalLink={dataExternalLink}
              userData={userData}
              formatMessage={formatMessage}
            />
          </Box>
        </Box>

        {/* Aside right col, only on desktop */}
        <Box as="aside" display={['none', undefined, undefined, undefined, 'block']} spaceY={4} bg="white">
          {/* Create account CTA */}
          {!userData && ( // if user is not connected
            <Box borderRadius="1rem" alignItems="center" py={4}>
              <A href="/signup" as="/signup">
                <Button>
                  {formatMessage({ id: 'program.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
                </Button>
              </A>
              <P pt={2} mb={0}>
                {formatMessage({
                  id: 'program.rightCompo.createAccount.text',
                  defaultMessage: 'to participate to the program',
                })}
              </P>
            </Box>
          )}
          {/* Supporters bloc */}
          {selectedTab === 'about' &&
          program?.enablers && ( // show only on about page
              <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
                <H2>{formatMessage({ id: 'program.supporters', defaultMessage: 'Supporters' })}</H2>
                <InfoHtmlComponent title="" content={program?.enablers} />
                <a href="mailto:hello@jogl.io">
                  {formatMessage({
                    id: 'program.supporters.interested',
                    defaultMessage: 'Interested in becoming a supporter?',
                  })}
                </a>
              </Box>
            )}
          {/* Latest announcements */}
          {selectedTab !== 'faq' && ( // show on all tabs except faq
            <Box pb={4}>
              <Box borderBottom="1px solid #d3d3d3" py={2}>
                <H2 mb={0}>
                  {formatMessage({
                    id: 'program.rightCompo.latestsAnnouncements',
                    defaultMessage: 'Latest announcements',
                  })}
                </H2>
              </Box>
              {!dataPosts ? (
                <Loading />
              ) : dataPosts?.length === 0 ? (
                <NoResults type="post" />
              ) : (
                <>
                  <Box>
                    {[...dataPosts].splice(0, 3).map((post, i) => (
                      <PostDisplayMinimal post={post} key={i} user={userData} refresh={revalidatePost} />
                    ))}
                  </Box>
                  <Box row p={2} justifyContent="center">
                    <A
                      href="/program/[short_title]?tab=news"
                      as={`/program/${router.query.short_title}?tab=news`}
                      shallow
                      scroll={false}
                    >
                      <Button>{formatMessage({ id: 'program.seeAll', defaultMessage: 'See all' })}</Button>
                    </A>
                  </Box>
                </>
              )}
            </Box>
          )}
          {/* Meeting info bloc */}
          {(selectedTab === 'home' || selectedTab === 'about') &&
          program?.meeting_information && ( // show only on home page
              <Box bg="white" display={['none', undefined, undefined, undefined, 'flex']} pt={3}>
                <H2>{formatMessage({ id: 'program.meeting_information', defaultMessage: "Let's meet!" })}</H2>
                <InfoHtmlComponent title="" content={program?.meeting_information} />
              </Box>
            )}
          {/* Contact program leaders bloc */}
          {selectedTab === 'faq' && members && (
            <Box display={['none', undefined, undefined, undefined, 'flex']} py={3}>
              <H2>{formatMessage({ id: 'footer.contactUs', defaultMessage: 'Contact us' })}</H2>
              <Box pt={5} spaceY={4}>
                {members
                  .filter(({ admin, owner }) => owner || admin) // only show leaders and admins
                  .map(({ id, first_name, last_name, logo_url_sm }, index) => (
                    <Box key={index} row spaceX={4} alignItems="center">
                      <Link href="/user/[id]" as={`/user/${id}`}>
                        <a>
                          <Box row spaceX={4} alignItems="center">
                            <img
                              width="50px"
                              height="50px"
                              style={{ borderRadius: '50%', objectFit: 'cover' }}
                              src={logo_url_sm}
                              alt={`${first_name} ${last_name}`}
                            />
                            <P fontWeight="bold" fontSize="1.2rem" mb={0}>
                              {first_name + ' ' + last_name}
                            </P>
                          </Box>
                        </a>
                      </Link>
                      <Box>
                        <ContactButton
                          icon="envelope"
                          size="lg"
                          onClick={() => sendMessageToAdmin(id, first_name, last_name)}
                          onKeyUp={(e) =>
                            // execute only if it's the 'enter' key
                            (e.which === 13 || e.keyCode === 13) && sendMessageToAdmin(id, first_name, last_name)
                          }
                          tabIndex={0}
                          data-tip={formatMessage(
                            {
                              id: 'user.contactModal.title',
                              defaultMessage: 'Send message to {userFullName}',
                            },
                            { userFullName: first_name + ' ' + last_name }
                          )}
                          data-for="contactAdmin"
                          // show/hide tooltip on element focus/blur
                          onFocus={(e) => ReactTooltip.show(e.target)}
                          onBlur={(e) => ReactTooltip.hide(e.target)}
                        />
                        <ReactTooltip id="contactAdmin" effect="solid" place="bottom" />
                      </Box>
                    </Box>
                  ))}
              </Box>
            </Box>
          )}
          {/* Invite someone component */}
          <Box pt={5}>
            <H2>{formatMessage({ id: 'member.invite.general', defaultMessage: 'Invite someone to join!' })}</H2>
            <InviteMember itemType="programs" itemId={program.id} />
          </Box>
          {/* Cta and links section (+ add style to make whole section sticky on top when attaining it */}
          <Box
            pt={7}
            display={['none', undefined, undefined, undefined, 'flex']}
            position={[undefined, undefined, undefined, undefined, 'sticky']}
            top={[undefined, undefined, undefined, undefined, '150px']}
          >
            {/* Create account CTA */}
            <CtaAndLinks
              router={router}
              contactEmail={contactEmail}
              dataExternalLink={dataExternalLink}
              userData={userData}
              formatMessage={formatMessage}
            />
          </Box>
        </Box>
      </Grid>
    </Layout>
  );
};

const CtaAndLinks = ({ router, contactEmail, dataExternalLink, userData, formatMessage }) => {
  const ExternalLinkIcon = styled(Box)`
    img:hover {
      opacity: 0.8;
    }
  `;
  return (
    <>
      {!userData && ( // if user is not connected
        <Box row alignItems={['start', undefined, undefined, undefined, 'center']} pt={4} pb={8} flexWrap="wrap">
          <A href="/signup" as="/signup">
            <Button>
              {formatMessage({ id: 'program.rightCompo.createAccount.btn', defaultMessage: 'Create an account' })}
            </Button>
          </A>
          <P pt={2} pl={2} mb={0}>
            {formatMessage({
              id: 'program.rightCompo.createAccount.text',
              defaultMessage: 'to participate to the program',
            })}
          </P>
        </Box>
      )}
      <H2>{formatMessage({ id: 'program.rightCompo.questions', defaultMessage: 'Questions?' })}</H2>
      <Box row justifyContent="flex-start" spaceX={3}>
        <QuestionBox>
          <A
            href="/program/[short_title]?tab=faq"
            as={`/program/${router.query.short_title}?tab=faq`}
            shallow
            scroll={false}
          >
            <FontAwesomeIcon icon="question-circle" />
            <P>{formatMessage({ id: 'faq.title', defaultMessage: 'FAQs' })}</P>
          </A>
        </QuestionBox>
        <QuestionBox>
          <A
            href="/program/[short_title]?tab=resources"
            as={`/program/${router.query.short_title}?tab=resources`}
            shallow
            scroll={false}
          >
            <FontAwesomeIcon icon="book-open" />
            <P>{formatMessage({ id: 'program.resourcesTitle', defaultMessage: 'Resources' })}</P>
          </A>
        </QuestionBox>
        <QuestionBox>
          <a href={`mailto:${contactEmail}`}>
            <FontAwesomeIcon icon="envelope" />
            <P>{formatMessage({ id: 'user.btn.contact', defaultMessage: 'Contact' })}</P>
          </a>
        </QuestionBox>
      </Box>
      {dataExternalLink && dataExternalLink?.length !== 0 && (
        <Box pt={8}>
          <H2 pb={4}>{formatMessage({ id: 'general.externalLink.findUs', defaultMessage: 'Find us' })}</H2>
          <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(57px, 1fr))">
            {[...dataExternalLink].map((link, i) => (
              <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                <a href={link.url} target="_blank">
                  <img width="45px" src={link.icon_url} />
                </a>
              </ExternalLinkIcon>
            ))}
          </Grid>
        </Box>
      )}
    </>
  );
};

const Tab = styled.a`
  display: inline-block;
  border-bottom: 3px solid transparent;
  border-bottom-color: ${(p) => p.selected && p.theme.colors.primary};
  font-weight: ${(p) => p.selected && 700};
  white-space: nowrap;
  color: inherit;
  text-align: right;
  cursor: pointer;
  &:hover {
    border-bottom-color: ${(p) => p.theme.colors.primary};
    color: inherit;
    text-decoration: none;
  }
  &:focus {
    outline: none;
  }
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid transparent;
    &:hover {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
    &:focus {
      border-bottom: 1px solid transparent;
      font-weight: 700;
    }
  }
  ${[space, layout]};
`;

const TabPanel = styled.div`
  ${[layout, space]};
  .infoHtml {
    width: 100% !important;
  }
`;

const NavContainer = styled(Box)`
  position: relative;
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    border-bottom: 1px solid ${(p) => p.theme.colors.greys['200']};
  }
  /* To make element sticky on top when reach top of page */
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    position: sticky;
    top: 150px;
  }

  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    width: 100%;
    justify-content: flex-end;
    padding-right: 20px;
  }
`;
const Nav = styled(Box)`
  overflow-x: scroll;
  @media (min-width: ${(p) => p.theme.breakpoints.md}) {
    font-size: ${(p) => p.theme.fontSizes.xl};
  }
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    padding-left: 1.8rem;
    button:last-child {
      padding-right: 3rem;
    }
    button {
      padding: 0 3px 5px;
    }
  }
`;

const OverflowGradient = styled.div`
  ${layout};
  height: 100%;
  position: absolute;
  ${(p) =>
    p.gradientPosition === 'right' &&
    'right:0;background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 3rem;'};
  ${(p) =>
    p.gradientPosition === 'left' &&
    'left:0;background: linear-gradient(90.82deg, white 50.95%, rgba(241, 244, 248, 0) 120.37%);width: 2rem;'};
`;

const QuestionBox = styled(Box)`
  border: 2px solid ${(p) => p.theme.colors.greys['800']};
  height: 6rem;
  width: 6rem;
  border-radius: 1rem;
  align-items: center;
  justify-content: center;
  background: white;
  cursor: pointer;
  svg {
    font-size: 2.3rem;
    color: ${(p) => p.theme.colors.greys['800']};
    margin-right: 0;
  }
  p {
    margin: 10px 0 0;
    font-size: 100%;
    font-weight: bold;
  }
  a {
    width: 100%;
    text-align: center;
  }
  &:hover {
    p,
    svg {
      color: ${(p) => p.theme.colors.primary};
    }
    border: 2px solid ${(p) => p.theme.colors.primary};
    a {
      text-decoration: none !important;
    }
  }
`;

const ContactButton = styled(FontAwesomeIcon)`
  color: ${(p) => p.theme.colors.greys['700']};
  :hover {
    cursor: pointer;
    color: ${(p) => p.theme.colors.primary};
  }
`;

const Text = styled(P)`
  ${[typography, layout]};
  margin-bottom: 0;
  font-size: 17.5px;
`;

const QuickAction = styled(Box)`
  justify-content: center;
  padding: 0 0 1.5rem;
  text-align: center;
  align-items: center;
  svg {
    margin-right: 0;
  }
  &:hover {
    color: ${(p) => p.theme.colors.greys['900']};
    cursor: pointer;
    > div {
      border-color: ${(p) => p.theme.colors.greys['900']};
      p,
      svg {
        color: ${(p) => p.theme.colors.greys['900']};
      }
    }
  }
`;

const QuickActionIcon = styled(Box)`
  border-radius: 50%;
  border: 2px solid ${(p) => p.theme.colors.greys['600']};
  width: 2.5rem;
  height: 2.5rem;
  justify-content: center;
  align-items: center;
`;

const Img = styled.img`
  ${[flexbox, layout, space]};
`;

const LogoImg = styled.img`
  ${[flexbox, space, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  width: 50px;
  height: 50px;
`;

// for desktop tabs that need an englobing div with padding and border
const DesktopBorders = ({ children }) => (
  <Box
    borderLeft={['none', undefined, '2px solid #f4f4f4']}
    borderRight={['none', undefined, undefined, undefined, '2px solid #f4f4f4']}
    pl={[0, undefined, 4]}
    pr={[0, undefined, undefined, undefined, 4]}
  >
    {children}
  </Box>
);

const getProgram = async (api, programId) => {
  const res = await api.get(`/api/programs/${programId}`).catch((err) => console.error(err));
  if (res?.data) {
    return res.data;
  }
  return undefined;
};

Program.getInitialProps = async ({ query, ...ctx }) => {
  const api = getApiFromCtx(ctx);
  // Case short_title is a string for pretty URL
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(Number(query.short_title as string))) {
    const res = await api.get(`/api/programs/getid/${query.short_title}`).catch((err) => console.error(err));
    if (res) {
      const program = await getProgram(api, res.data.id).catch((err) => console.error(err));
      return { program };
    }
    isomorphicRedirect(ctx, '/', '/');
  }
  // Case short_title is actually an id
  const program = await getProgram(api, query.short_title).catch((err) => console.error(err));
  if (program) {
    return { program };
  } else {
    isomorphicRedirect(ctx, '/', '/');
  }
};

export default Program;
