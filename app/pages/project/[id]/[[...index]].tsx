import $ from 'jquery';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import Box from '~/components/Box';
import Feed from '~/components/Feed/Feed';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import NeedCard from '~/components/Need/NeedCard';
import NeedCreate from '~/components/Need/NeedCreate';
import H2 from '~/components/primitives/H2';
import ProjectHeader from '~/components/Project/ProjectHeader';
import Alert from '~/components/Tools/Alert';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import InfoHtmlComponent from '~/components/Tools/Info/InfoHtmlComponent';
import Loading from '~/components/Tools/Loading';
import NoResults from '~/components/Tools/NoResults';
import useGet from '~/hooks/useGet';
import useNeeds from '~/hooks/useNeeds';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import { getApiFromCtx } from '~/utils/getApi';
import isomorphicRedirect from '~/utils/isomorphicRedirect';
import styled from '~/utils/styled';
// import "Components/Main/Similar.scss";
import { scrollToActiveTab, stickyTabNav } from '~/utils/utils';

interface Props {
  project: Project;
}
const ProjectDetails: NextPage<Props> = ({ project }) => {
  const { userData } = useUserData();
  const { formatMessage } = useIntl();
  const router = useRouter();
  const [isMember, setIsMember] = useState(false);
  const { dataNeeds, needsRevalidate } = useNeeds('projects', project?.id);
  const { data: dataExternalLink } = useGet(`/api/projects/${project?.id}/links`);

  useEffect(() => {
    const urlSuccessParam = router.query.success;
    setTimeout(() => {
      stickyTabNav(router); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(router); // if there is a hash in the url and the tab exists, click and scroll to the tab
      if (urlSuccessParam === '1') {
        // if url success param is 1, show "saved changes" success alert
        $('#editSuccess').show(); // display success message
        setTimeout(() => {
          $('#editSuccess').hide(300);
        }, 2000); // hide it after 2sec
      }
    }, 1200); // had to add setTimeout to make it work
  }, [router]);
  useEffect(() => {
    (project.is_member || router.query.tab === 'feed') && setIsMember(true);
  }, [project.is_member]);
  // Dynamic SEO meta tags
  return (
    <Layout
      title={project?.title && `${project.title} | JOGL`}
      desc={project?.short_description}
      img={project?.banner_url || '/images/default/default-project.jpg'}
    >
      {project && (
        <div className="projectDetails container-fluid">
          <ProjectHeader project={project} />
          <nav className="nav nav-tabs container-fluid">
            <a className={`nav-item nav-link ${isMember ? 'active' : ''}`} href="#news" data-toggle="tab">
              <FormattedMessage id="entity.tab.news" defaultMessage="Feed" />
            </a>
            <a className={`nav-item nav-link ${!isMember ? 'active' : ''}`} href="#about" data-toggle="tab">
              <FormattedMessage id="project.tab.about" defaultMessage="About" />
            </a>
            <a className="nav-item nav-link" href="#needs" data-toggle="tab">
              <FormattedMessage id="entity.tab.needs" defaultMessage="Needs" />
            </a>
            {((project.documents.length !== 0 && (!project.is_admin || !userData)) || project.is_admin) && ( // hide documents tab is user is not admin and they are no docs
              <a className="nav-item nav-link" href="#documents" data-toggle="tab">
                <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
              </a>
            )}
          </nav>
          <div className="tabContainer">
            <div className="tab-content justify-content-center container-fluid">
              <div className={`tab-pane ${isMember ? 'active' : ''}`} id="news">
                {project.feed_id && (
                  <Feed
                    feedId={project.feed_id}
                    // display post create component if you are member of the group
                    displayCreate={project.is_member}
                    isAdmin={project.is_admin}
                    needToJoinMsg={project.is_private}
                  />
                )}
              </div>
              <div className={`tab-pane ${!isMember ? 'active' : ''}`} id="about">
                <Box width={['100%', undefined, undefined, '70%']} margin="auto">
                  {project.description ? (
                    <InfoHtmlComponent title="" content={project.description} />
                  ) : (
                    project.short_description
                  )}
                  {project.desc_elevator_pitch && (
                    <h4>
                      <FormattedMessage
                        id="entity.info.desc_elevator_pitch"
                        defaultMessage="Elevator pitch / Abstract"
                      />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_elevator_pitch} />
                  {project.desc_contributing && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_contributing" defaultMessage="How to contribute" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_contributing} />
                  {project.desc_problem_statement && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_problem_statement" defaultMessage="Problem Statement" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_problem_statement} />
                  {project.desc_objectives && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_objectives" defaultMessage="Objectives & Methodology" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_objectives} />
                  {project.desc_state_art && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_state_art" defaultMessage="State of the art" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_state_art} />
                  {project.desc_progress && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_progress" defaultMessage="Progress report" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_progress} />
                  {project.desc_stakeholder && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_stakeholder" defaultMessage="Stakeholders" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_stakeholder} />
                  {project.desc_impact_strat && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_impact_strat" defaultMessage="Impact strategy" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_impact_strat} />
                  {project.desc_ethical_statement && (
                    <h4>
                      <FormattedMessage
                        id="entity.info.desc_ethical_statement"
                        defaultMessage="Ethical considerations"
                      />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_ethical_statement} />
                  {project.desc_sustainability_scalability && (
                    <h4>
                      <FormattedMessage
                        id="entity.info.desc_sustainability_scalability"
                        defaultMessage="Sustainability and scalability"
                      />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_sustainability_scalability} />
                  {project.desc_communication_strat && (
                    <h4>
                      <FormattedMessage
                        id="entity.info.desc_communication_strat"
                        defaultMessage="Communication and dissemination strategy"
                      />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_communication_strat} />
                  {project.desc_funding && (
                    <h4>
                      <FormattedMessage id="entity.info.desc_funding" defaultMessage="Funding" />
                    </h4>
                  )}
                  <InfoHtmlComponent title="" content={project.desc_funding} />
                  {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <Box pt={8}>
                      <H2 pb={4}>{formatMessage({ id: 'general.externalLink.findUs', defaultMessage: 'Find us' })}</H2>
                      <Grid display="grid" gridTemplateColumns="repeat(auto-fill, minmax(55px, 1fr))">
                        {[...dataExternalLink].map((link, i) => (
                          <ExternalLinkIcon alignSelf="center" key={i} pb={3}>
                            <a href={link.url} target="_blank">
                              <img width="45px" src={link.icon_url} />
                            </a>
                          </ExternalLinkIcon>
                        ))}
                      </Grid>
                    </Box>
                  )}
                </Box>
              </div>
              <div className="tab-pane" id="needs">
                <>
                  {!!project.is_admin && <NeedCreate projectId={project.id} refresh={needsRevalidate} />}
                  <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
                    {!dataNeeds ? (
                      <Loading />
                    ) : dataNeeds.length === 0 ? (
                      <NoResults type="need" />
                    ) : (
                      [...dataNeeds]
                        .reverse()
                        .map((need, i) => (
                          <NeedCard
                            key={i}
                            title={need.title}
                            skills={need.skills}
                            resources={need.ressources}
                            hasSaved={need.has_saved}
                            id={need.id}
                            postsCount={need.posts_count}
                            membersCount={need.members_count}
                            publishedDate={need.created_at}
                            dueDate={need.end_date}
                            status={need.status}
                          />
                        ))
                    )}
                  </Grid>
                </>
              </div>
              <div className="tab-pane" id="documents">
                <DocumentsManager item={project} itemId={project.id} itemType="projects" />
              </div>
            </div>
          </div>
          <Alert
            type="success"
            id="editSuccess"
            message={
              <FormattedMessage
                id="general.editSuccessMsg"
                defaultMessage="The changes have been saved successfully."
              />
            }
          />
        </div>
      )}
    </Layout>
  );
};
const ExternalLinkIcon = styled(Box)`
  img:hover {
    opacity: 0.8;
  }
`;

ProjectDetails.getInitialProps = async (ctx) => {
  const api = getApiFromCtx(ctx);
  const res = await api.get(`/api/projects/${ctx.query.id}`).catch((err) => console.error(err));
  if (res) return { project: res.data };
  isomorphicRedirect(ctx, '/search/[active-index]', '/search/projects');
};

export default ProjectDetails;
