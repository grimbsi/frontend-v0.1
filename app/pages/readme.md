# Routing

Next.js has a file-system based router built on the concept of pages.

When a file is added to the pages directory it's automatically available as a route.

The files inside the pages directory can be used to define most common patterns.

## Index routes

The router will automatically route files named index to the root of the directory.

- pages/index.js → /
- pages/blog/index.js → /blog

## Nested routes

The router supports nested files. If you create a nested folder structure files will be automatically routed in the same way still.

- pages/blog/first-post/index.js → /blog/first-post
- pages/dashboard/settings/username/index.js → /dashboard/settings/username

## Dynamic route segments

To match a dynamic segment you can use the bracket syntax. This allows you to match named parameters.

- pages/blog/[slug]/index.js → /blog/:slug (/blog/hello-world)
- pages/[username]/settings/index.js → /:username/settings (/foo/settings)
- pages/post/[...all]/index.js → /post/\* (/post/2020/id/title)

## Forced structure

To keep a constant architecture we decided to always use folders and index.js for every single page.

### Incorrect

- pages/blog/[slug].js → /blog/:slug (/blog/hello-world)
- pages/blog/first-post.js → /blog/first-post

### Correct

- pages/blog/[slug]/index.js → /blog/:slug (/blog/hello-world)
- pages/blog/first-post/index.js → /blog/first-post

For more information visit: [Next.js routing introduction](https://nextjs.org/docs/routing/introduction)
