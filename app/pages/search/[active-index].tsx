import { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import Router, { useRouter, NextRouter } from 'next/router';
import qs from 'qs';
import Search from '~/components/Search/Search';
import Layout from '~/components/Layout';
import { NextPage } from 'next';

const pathToSearchState = (asPath: NextRouter['asPath']) =>
  asPath.includes('?') ? qs.parse(asPath.substring(asPath.indexOf('?') + 1)) : {};

const searchStateToURL = (searchState, index) => {
  // Prevent the trailing ? when searchState is empty.
  const isSearchStateEmpty = Object.keys(searchState).length === 0 && searchState.constructor === Object;
  return isSearchStateEmpty ? `/search/${index}` : `/search/${index}?${qs.stringify(searchState)}`;
};
interface Props {}
const SearchPageIndices: NextPage<Props> = () => {
  const { query, ...router } = useRouter();
  const { 'active-index': activeIndex } = query;
  const [index, setIndex] = useState(activeIndex);
  // Possible states: Members, Needs, Projects, Groups, Challenges
  const indices = [
    { value: 'members', name: 'User', intlId: 'general.members' },
    { value: 'needs', name: 'Need', intlId: 'entity.tab.needs' },
    { value: 'projects', name: 'Project', intlId: 'general.projects' },
    { value: 'groups', name: 'Community', intlId: 'general.groups' },
    { value: 'challenges', name: 'Challenge', intlId: 'general.challenges' },
  ];
  // Initialize searchState with params from the URL
  const [searchState, setSearchState] = useState(() => pathToSearchState(router.asPath));
  const onSearchStateChange = (newSearchState) => {
    setSearchState(newSearchState);
  };
  useEffect(() => {
    // Update URL params when searchState changes
    const href = searchStateToURL(searchState, index);
    Router.replace('/search/[active-index]', href, { shallow: true });
  }, [index, searchState]);

  useEffect(() => {
    // Keep only the refinementList if present
    setSearchState((prevState) => ({
      ...(prevState?.refinementList ? { refinementList: prevState.refinementList } : {}),
      ...(prevState?.query ? { query: prevState.query } : {}),
    }));
    // Update URL params when searchState changes
    setIndex(activeIndex);
  }, [activeIndex]);

  const { formatMessage } = useIntl();

  return (
    <Layout title={`Search ${index} | JOGL`}>
      <div className="searchPageAll AlgoliaResulstsPages container-fluid">
        <nav className="nav container-fluid">
          {indices.map(({ value, intlId }, key) => (
            <TabLink
              key={key}
              indexName={value}
              onClick={(newIndex) => {
                setIndex(newIndex);
              }}
              active={value === index}
            >
              {formatMessage({ id: intlId, defaultMessage: value })}
            </TabLink>
          ))}
        </nav>
        <div className="tabContainer">
          <div className="tab-content">
            {index === 'members' && (
              <div className="tab-pane active" id="members">
                <Search
                  searchState={searchState}
                  index="User"
                  onSearchStateChange={onSearchStateChange}
                  refinements={[
                    { attribute: 'skills' },
                    { attribute: 'ressources' },
                    { attribute: 'interests', searchable: false, limit: 17 },
                  ]}
                  sortByItems={[
                    {
                      label: formatMessage({
                        id: 'general.filter.people.date2',
                        defaultMessage: 'Signup date (newest)',
                      }),
                      index: 'User_id_des',
                    },
                    { label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }), index: 'User' },
                    {
                      label: formatMessage({
                        id: 'general.filter.people.date1',
                        defaultMessage: 'Signup date (oldest)',
                      }),
                      index: 'User_id_asc',
                    },
                    {
                      label: formatMessage({ id: 'general.filter.people.alpha1', defaultMessage: 'Last name (A-Z)' }),
                      index: 'User_fname_asc',
                    },
                    {
                      label: formatMessage({ id: 'general.filter.people.alpha2', defaultMessage: 'Last name (Z-A)' }),
                      index: 'User_fname_desc',
                    },
                  ]}
                />
              </div>
            )}
            {index === 'needs' && (
              <div className="tab-pane active" id={index}>
                <Search
                  searchState={searchState}
                  index="Need"
                  onSearchStateChange={onSearchStateChange}
                  refinements={[
                    { attribute: 'skills' },
                    { attribute: 'ressources' },
                    { attribute: 'project.title' },
                    { attribute: 'status', searchable: false, showmore: false },
                  ]}
                  sortByItems={[
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date2',
                        defaultMessage: 'Date added (newest)',
                      }),
                      index: 'Need_id_des',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date1',
                        defaultMessage: 'Date added (oldest)',
                      }),
                      index: 'Need_id_asc',
                    },
                    { label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }), index: 'Need' },
                    {
                      label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
                      index: 'Need_members',
                    },
                  ]}
                />
              </div>
            )}
            <div className="tab-pane active" id="projects">
              {index === 'projects' && (
                <Search
                  searchState={searchState}
                  index="Project"
                  onSearchStateChange={onSearchStateChange}
                  refinements={[
                    { attribute: 'programs.title', searchable: false },
                    { attribute: 'challenges.title' },
                    { attribute: 'skills' },
                    { attribute: 'interests', searchable: false },
                  ]}
                  sortByItems={[
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date2',
                        defaultMessage: 'Date added (newest)',
                      }),
                      index: 'Project_id_des',
                    },
                    {
                      label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
                      index: 'Project',
                    },
                    {
                      label: formatMessage({ id: 'needs.uppercase', defaultMessage: 'Needs' }),
                      index: 'Project_needs',
                    },
                    {
                      label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
                      index: 'Project_members',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date1',
                        defaultMessage: 'Date added (oldest)',
                      }),
                      index: 'Project_id_asc',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.alpha1',
                        defaultMessage: 'Alphabetical (A-Z)',
                      }),
                      index: 'Project_title_asc',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.alpha2',
                        defaultMessage: 'Alphabetical (Z-A)',
                      }),
                      index: 'Project_title_desc',
                    },
                  ]}
                />
              )}
            </div>
            {index === 'groups' && (
              <div className="tab-pane active" id="groups">
                <Search
                  searchState={searchState}
                  index="Community"
                  onSearchStateChange={onSearchStateChange}
                  refinements={[{ attribute: 'skills' }, { attribute: 'ressources' }]}
                  sortByItems={[
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date2',
                        defaultMessage: 'Date added (newest)',
                      }),
                      index: 'Community_id_des',
                    },
                    {
                      label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
                      index: 'Community',
                    },
                    {
                      label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
                      index: 'Community_members',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date1',
                        defaultMessage: 'Date added (oldest)',
                      }),
                      index: 'Community_id_asc',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.alpha1',
                        defaultMessage: 'Alphabetical (A-Z)',
                      }),
                      index: 'Community_title_asc',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.alpha2',
                        defaultMessage: 'Alphabetical (Z-A)',
                      }),
                      index: 'Community_title_desc',
                    },
                  ]}
                />
              </div>
            )}
            {index === 'challenges' && (
              <div className="tab-pane active" id="challenges">
                <Search
                  searchState={searchState}
                  index="Challenge"
                  onSearchStateChange={onSearchStateChange}
                  refinements={[
                    { attribute: 'skills' },
                    { attribute: 'program.title', searchable: false },
                    { attribute: 'status', searchable: false, showmore: false },
                    { attribute: 'interests', searchable: false },
                  ]}
                  sortByItems={[
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date2',
                        defaultMessage: 'Date added (newest)',
                      }),
                      index: 'Challenge_id_des',
                    },
                    {
                      label: formatMessage({ id: 'general.filter.pop', defaultMessage: 'Popularity' }),
                      index: 'Challenge',
                    },
                    {
                      label: formatMessage({ id: 'general.members', defaultMessage: 'Members' }),
                      index: 'Challenge_members',
                    },
                    {
                      label: formatMessage({ id: 'general.projects', defaultMessage: 'Projects' }),
                      index: 'Challenge_projects',
                    },
                    {
                      label: formatMessage({ id: 'needs.uppercase', defaultMessage: 'Needs' }),
                      index: 'Challenge_needs',
                    },
                    {
                      label: formatMessage({
                        id: 'general.filter.object.date1',
                        defaultMessage: 'Date added (oldest)',
                      }),
                      index: 'Challenge_id_asc',
                    },
                  ]}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
};

const TabLink = ({ indexName, children, active, onClick }) => (
  <div
    className={`indexType ${active ? 'active' : ''}`}
    data-toggle="tab"
    onClick={() => onClick(indexName)}
    onKeyDown={() => onClick(indexName)}
    role="link"
    tabIndex={0}
  >
    {children}
  </div>
);

export default SearchPageIndices;
