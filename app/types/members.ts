import { Geoloc } from './common';

export interface Members {
  users: {
    id: number;
    first_name: string;
    last_name: string;
    owner: boolean;
    admin: boolean;
    member: boolean;
    has_followed: boolean;
    has_clapped: boolean;
    logo_url: string;
    logo_url_sm: string;
    short_bio: string;
    geoloc: Geoloc;
    can_contact: boolean;
    bio: string;
  }[];
}
