import { Creator } from './common';

interface Mention {
  obj_type: string;
  obj_id: number;
  obj_match: string;
}

interface From {
  object_type: string;
  object_id: number;
  object_name: string;
  object_image?: any;
  object_need_proj_id?: any;
}

interface Clapper {
  id?: any;
  user_id: number;
}

export interface Post {
  id: number;
  content: string;
  media?: any;
  creator: Creator;
  mentions: Mention[];
  from: From;
  comments: any[];
  created_at: Date;
  documents: any[];
  claps_count: number;
  saves_count: number;
  clappers: Clapper[];
  has_clapped: boolean;
  has_saved: boolean;
}
