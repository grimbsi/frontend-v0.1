import { Challenge } from './challenge';
import { Creator, Geoloc, UsersSm } from './common';
import { Program } from './program';

export interface Project {
  id: number;
  title: string;
  short_title: string;
  banner_url: string;
  banner_url_sm: string;
  short_description: string;
  creator: Creator;
  status: string;
  skills: any[];
  interests: any[];
  badges: any[];
  geoloc: Geoloc;
  country?: any;
  city?: any;
  address?: any;
  feed_id: number;
  is_private: boolean;
  challenges: Pick<
    Challenge,
    'banner_url' | 'id' | 'short_description' | 'short_description_fr' | 'short_title' | 'status' | 'title' | 'title_fr'
  >[];
  programs: Pick<Program, 'id' | 'short_title' | 'title'>[];
  users_sm: UsersSm[];
  challenge_status: string;
  claps_count: number;
  follower_count: number;
  saves_count: number;
  members_count: number;
  needs_count: number;
  created_at: Date;
  updated_at: Date;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  is_pending: boolean;
  has_clapped: boolean;
  has_followed: boolean;
  has_saved: boolean;
  documents: any[];
  documents_feed: any[];
  description: string;
  desc_elevator_pitch?: any;
  desc_problem_statement?: any;
  desc_objectives?: any;
  desc_state_art?: any;
  desc_progress?: any;
  desc_stakeholder?: any;
  desc_impact_strat?: any;
  desc_ethical_statement?: any;
  desc_sustainability_scalability?: any;
  desc_communication_strat?: any;
  desc_funding?: any;
  desc_contributing?: any;
  challenge_id?: number;
}
