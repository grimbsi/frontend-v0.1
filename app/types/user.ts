interface Badge {
  id: number;
  name: string;
  description: string;
  custom_fields?: any;
}

export interface User {
  id: number;
  first_name: string;
  last_name: string;
  nickname: string;
  affiliation: string;
  category: string;
  country: string;
  city: string;
  bio?: any;
  short_bio: string;
  can_contact: boolean;
  status: string;
  confirmed_at: Date;
  interests: number[];
  skills: string[];
  ressources: string[];
  badges: Badge[];
  feed_id: number;
  logo_url: string;
  logo_url_sm: string;
  claps_count: number;
  saves_count: number;
  follower_count: number;
  following_count: number;
  mail_weekly: boolean;
  mail_newsletter: boolean;
  email_notifications_enabled: boolean;
  is_admin: boolean;
  has_clapped: boolean;
  has_followed: boolean;
  has_saved: boolean;
  geoloc: {
    lat: number;
    lng: number;
  };
}
