import Router from 'next/router';

export default function isomorphicRedirect(ctx, path, asPath) {
  if (typeof window !== 'undefined') {
    Router.push(path, asPath);
  } else {
    ctx.res.writeHead(302, { Location: asPath || path });
    ctx.res.end();
  }
  // Return empty object because getInitialProps doesn't allow null returns
  return {};
}
