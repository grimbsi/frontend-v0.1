import { useIntl } from 'react-intl';

export const textWithPlural = (type, count) => {
  const { formatMessage } = useIntl();
  switch (type) {
    case 'participant':
      return count > 1
        ? formatMessage({ id: 'entity.tab.participants', defaultMessage: 'Participants' })
        : formatMessage({ id: 'entity.info.participants', defaultMessage: 'Participant' });
    case 'need':
      return count > 1
        ? formatMessage({ id: 'needs.uppercase', defaultMessage: 'Needs' })
        : formatMessage({ id: 'need.uppercase', defaultMessage: 'Need' });
    case 'project':
      return count > 1
        ? formatMessage({ id: 'general.projects', defaultMessage: 'Projects' })
        : formatMessage({ id: 'project', defaultMessage: 'Project' });
    case 'member':
      return count > 1
        ? formatMessage({ id: 'member.role.members', defaultMessage: 'Members' })
        : formatMessage({ id: 'member.role.member', defaultMessage: 'Member' });
    case 'clap':
      return count > 1
        ? formatMessage({ id: 'general.claps', defaultMessage: 'Claps' })
        : formatMessage({ id: 'general.clap', defaultMessage: 'Clap' });
    case 'following':
      return count > 1
        ? formatMessage({ id: 'user.profile.followings', defaultMessage: 'Followings' })
        : formatMessage({ id: 'user.profile.following', defaultMessage: 'Following' });
    case 'follower':
      return count > 1
        ? formatMessage({ id: 'user.profile.followers', defaultMessage: 'Followers' })
        : formatMessage({ id: 'user.profile.follower', defaultMessage: 'Follower' });
    case 'post':
      return count > 1
        ? formatMessage({ id: 'post.posts', defaultMessage: 'Posts' })
        : formatMessage({ id: 'post.post', defaultMessage: 'Post' });
    case 'comment':
      return count > 1
        ? formatMessage({ id: 'post.comments', defaultMessage: 'Comments' })
        : formatMessage({ id: 'post.comment', defaultMessage: 'Comment' });
    case 'volunteer':
      return count > 2
        ? formatMessage({ id: 'needs.volunteers', defaultMessage: 'volunteers' })
        : formatMessage({ id: 'needs.volunteer', defaultMessage: 'volunteer' });
    case 'mutualConnection':
      return count > 1
        ? formatMessage({ id: 'user.info.mutualConnections', defaultMessage: 'mutual connections' })
        : formatMessage({ id: 'user.info.mutualConnection', defaultMessage: 'mutual connection' });
    default:
      return undefined;
  }
};
