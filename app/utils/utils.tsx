import React, { Fragment, useRef, useEffect, useState } from 'react';
import { FormattedMessage, FormattedDate, FormattedRelativeTime } from 'react-intl';
import Link from 'next/link';
import { selectUnit } from '@formatjs/intl-utils';
// import ReactGA from 'react-ga';
// import $ from "jquery";

export function linkify(content) {
  // detect links in a text and englobe them with a <a> tag
  const urlRegex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm;
  return content.replace(urlRegex, (url) => {
    const addHttp = url.substr(0, 4) !== 'http' ? 'http://' : '';
    return `<a href="${addHttp}${url}" target="_blank" rel="noopener">${url}</a>`;
  });
}

export function linkifyQuill(text) {
  // detect links outside of <a> tag and linkify them (only for quill content)
  const exp = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  return text.replace(exp, function () {
    return arguments[1] ? arguments[0] : `<a href="${arguments[3]}" target='_blank' rel="noopener">${arguments[3]}</a>`;
  });
}

export function stickyTabNav(isEdit) {
  // Add the sticky class to the tab navigation when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function stickyScrollTop(sticky) {
    if (window.pageYOffset >= sticky) {
      // when we arrive at the level of the tab navigation div
      navtabs.classList.add('sticky'); // add class sticky
    } else {
      navtabs.classList.remove('sticky');
    }
  }
  // Get the tab navigation
  const navtabs = document.querySelector('.nav-tabs');
  // Get the offset position of the tab navigation
  if (navtabs) {
    // check if nav
    const sticky = navtabs.offsetTop - 80; // equals to the offset from top of navtab - 80px (header height)
    // When the user scrolls the page, execute stickyScrollTop
    stickyScrollTop(sticky); // launch function once
    window.onscroll = function () {
      stickyScrollTop(sticky);
    }; // redo function on scroll

    $('.nav-tabs .nav-item.nav-link').on('click', () => {
      window.scrollTo({ top: sticky, behavior: 'smooth' }); // stickyValue is dynamically 0 or sticky depending if the tab navigation is sticky
      // TODO fix this
      // if (!isEdit) hash = $(this)[0].hash;
    });
  }
}

export const useScrollHandler = (elOffSetTop: number) => {
  // setting initial value to true
  const [scroll, setScroll] = useState(false);
  // running on mount
  useEffect(() => {
    const onScroll = () => {
      const scrollCheck = window.scrollY > elOffSetTop;
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck);
      }
    };
    // setting the event handler from web API
    document.addEventListener('scroll', onScroll);
    // cleaning up from the web API
    return () => {
      document.removeEventListener('scroll', onScroll);
    };
  }, [scroll, setScroll, elOffSetTop]);
  return scroll;
};

export function scrollToActiveTab(router) {
  const hash =
    typeof window !== 'undefined' && router.asPath.match(/#([a-z0-9]+)/gi)
      ? router.asPath.match(/#([a-z0-9]+)/gi)[0]
      : window.location.hash;
  // takes the hash() in the url, if it match the href of a tab, then force click on the tab to access content
  if (hash && document.body.contains(document.querySelector(`a[href="${hash}"]`))) {
    $(`a[href="${hash}"]`)[0].click();
    const element = document.querySelector('.tab-pane.active'); // get active tab content
    const y = element.getBoundingClientRect().top + window.pageYOffset - 200; // calculate it's top value and remove 140 of offset
    window.scrollTo({ top: y, behavior: 'smooth' }); // scroll to tab
  }
}

export function renderOwnerNames(users, projectCreator) {
  let noOwner = true;
  users.map((user) => {
    if (user.owner) noOwner = false;
  }); // map through users, and set noOwner to false if there are no owners
  return (
    <p className="card-by">
      <FormattedMessage id="entity.card.by" defaultMessage="by " />
      {users
        .filter((user) => user.owner)
        .map((user, index, users) => {
          // filter to get only owners, and then map
          const rowLen = users.length;
          if (index < 6) {
            return (
              <Fragment key={index}>
                <Link href="/user/[id]" as={`/user/${user.id}`}>
                  <a>{`${user.first_name} ${user.last_name}`}</a>
                </Link>
                {/* add comma, except for last item */}
                {rowLen !== index + 1 && <span>, </span>}
              </Fragment>
            );
          }
          if (index === 6) {
            return '...';
          }
          return '';
        })}
      {noOwner && ( // if project don't have any owner, display creator as the owner
        <Link href="/user/[id]" as={`/user/${projectCreator.id}`}>
          <a>{`${projectCreator.first_name} ${projectCreator.last_name}`}</a>
        </Link>
      )}
    </p>
  );
}

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function displayObjectDate(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  // check if browser is safari or IE
  var isSafari,
    isIE = false;
  if (typeof window !== 'undefined') {
    isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    isIE = /*@cc_on!@*/ false || !!document.documentMode;
  }
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // if date diff is less than 1h (3600ec), display related date (.. minutes ago, with update every x seconds)
  if (dateDiff < 3600 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} updateIntervalInSeconds={60} />;
  // else, if date diff less than 1 day (3600sec * 24h), display related date without time update
  if (dateDiff < 86400 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} />;
  // else display date with day, month & year
  return <FormattedDate value={objectCreatedDate} />;
}

export function displayObjectRelativeDate(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  // check if browser is safari or IE
  var isSafari,
    isIE = false;
  if (typeof window !== 'undefined') {
    isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    isIE = /*@cc_on!@*/ false || !!document.documentMode;
  }
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // if date diff is less than 1h (3600ec), display related date (.. minutes ago, with update every x seconds)
  if (dateDiff < 3600 && !isSafari && !isIE)
    return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} updateIntervalInSeconds={60} />;
  // else, if date diff less than 1 day (3600sec * 24h), display related date without time update
  if (!isSafari && !isIE) return <FormattedRelativeTime numeric="auto" value={objectDate} unit={objectUnit} />;
  // else display date with day, month & year
  return <FormattedDate value={objectCreatedDate} />;
}

export function isDateUrgent(date) {
  const objectCreatedDate = new Date(date);
  const { value: objectDate, unit: objectUnit } = selectUnit(objectCreatedDate);
  const dateDiff = Math.abs(objectCreatedDate - new Date()) / 1000; // get date difference
  // else, if date diff less than 2 days (3600sec * 24h * 2), display related date without time update
  if (dateDiff < 86400) return true;
}

// return first link of a post (the one we want the metadata from)
export function returnFirstLink(content) {
  if (content) {
    const regexLinks = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm; // match all links
    const match = content.match(regexLinks);
    if (match) {
      const firstLink = match[0];
      return firstLink;
    }
  }
}

export function copyLink(objectId, objectType, commentId) {
  // function to copy post link to user clipboard, we use a tweak because the browser don't like we force copy to clipboard
  const el = document.createElement('textarea'); // create textarea element
  const link =
    objectType === 'post'
      ? `${window.location.origin}/${objectType}/${objectId}` // if it's a post, make its value be the post single url
      : objectType === 'comment' && `${window.location.origin}/post/${objectId}/#comment-${commentId}`; // if it' a comment, make its value be the comment single url
  el.value = link;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute'; // put some style that make it not visible
  el.style.left = '-9999px';
  document.body.appendChild(el); // add element to html (body)
  el.select(); // select the textarea text
  document.execCommand('copy'); // copy content to user clipboard
  document.body.removeChild(el); // remove element
  const alertId = commentId ? commentId : objectId;
  $(`.alert-${alertId}#copyConfirmation`).show(); // display "object was copied" alert
  setTimeout(() => {
    $(`.alert-${alertId}#copyConfirmation`).hide(300);
  }, 3000); // hide it after 2sec
}

export function reportContent(contentType, postId, commentId, api) {
  // function to send mail with the reported post to JOGL admin
  const alertType = contentType === 'post' ? 'reportPostConfirmation' : 'reportCommentConfirmation';
  const param =
    contentType === 'post'
      ? {
          // if it's post
          object: 'Post report', // mail subject
          content: `The user reported the following post: ${window.location.origin}/post/${postId}`, // mail content, with reported post link
        }
      : {
          // if it's comment
          object: 'Comment report', // mail subject
          content: `The user reported the following comment: ${window.location.origin}/post/${postId}#comment-${commentId}`, // mail content, with reported post link
        };
  api
    .post('/api/users/2/send_email', param) // send it to user 2, which is JOGL admin user
    .then((res) => {
      const alertId = commentId ? commentId : postId;
      $(`.alert-${alertId}#${alertType}`).show(); // display "post was reported" alert
      setTimeout(() => {
        $(`.alert-${alertId}#${alertType}`).hide(300);
      }, 2000); // hide it after 2sec
    })
    .catch((error) => {});
}

export function useWhyDidYouUpdate(name, props) {
  // Get a mutable ref object where we can store props ...
  // ... for comparison next time this hook runs.
  const previousProps = useRef();

  useEffect(() => {
    if (previousProps.current) {
      // Get all keys from previous and current props
      const allKeys = Object.keys({ ...previousProps.current, ...props });
      // Use this object to keep track of changed props
      const changesObj = {};
      // Iterate through keys
      allKeys.forEach((key) => {
        // If previous is different from current
        if (previousProps.current[key] !== props[key]) {
          // Add to changesObj
          changesObj[key] = {
            from: previousProps.current[key],
            to: props[key],
          };
        }
      });

      // If changesObj not empty then output to console
      if (Object.keys(changesObj).length) {
        // console.log('[why-did-you-update]', name, changesObj);
      }
    }

    // Finally update previousProps with current props for next hook call
    previousProps.current = props;
  });
}
